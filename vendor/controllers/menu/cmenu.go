package cmenu

import (
	"log"
	"encoding/json"
	// "strconv"
	// "time"

	"utils"
	"constants"
	"hare/envi"
	"global"
	
	"github.com/gin-gonic/gin"
)

var env = envi.Config
var tmpl = constants.TmplRoot
var tmpl_site = constants.TmplSite
var tmpl_common = constants.TmplCommon
type DataMap map[string]interface{}

func Show(c *gin.Context) {
	id := util.Decrypt(c.Param("id"), constants.AesKeyTable)
	order_finish := c.Request.URL.Query().Get("order")
	table_pass := c.Request.URL.Query().Get("p")

	// Check table pass
	table_auth := "0"
	if table_pass != "" {
		rb, _ := json.Marshal(DataMap{
			"table_id": id,
			"table_password": util.Decrypt(table_pass, constants.AesKeyTablePass),
		})
		resPass := util.ReqPost(env.API_HARE_URL + "tablepass/check", env.API_HARE_TOKEN, rb)
		if resPass["status"] == 200.0  {
			table_auth = "1"
		} else {
			table_auth = "0"
		}
	}
	// log.Println(util.FloatToString(resPass["status"].(float64)) + "ASDSA" + table_auth)

	// Get table menu
	res := util.SendReqGet(env.API_HARE_URL + "table/" + id, env.API_HARE_TOKEN)

	var resMap DataMap
	json.Unmarshal([]byte(res), &resMap)
	log.Println(resMap)
	resStatus := int(resMap["status"].(float64))
	
	if resStatus == 200 {
		resDataMenu := resMap["data_menu"]

		// Count total
		var order_total float64
		order_total = 0

		resOrder := resMap["data_order"].(map[string]interface{})
		// log.Println("TYPE" + util.FloatToString(resOrder["package_type"].(float64)))

		if resOrder["order"] != nil {
			arrOrder := resOrder["order"].([]interface{})

			for _, ord := range arrOrder {
				// od, ok := rec.(map[string]interface{})
				objOrder := ord.(map[string]interface{})
				arrMenu := objOrder["menu"].([]interface{})

				for _, men := range arrMenu {
					objMenu := men.(map[string]interface{})
					mqty := objMenu["qty"].(float64)
					mprice := objMenu["order_menu_price"].(float64)
					mtotal := mqty * mprice
					order_total += mtotal
				}
			}
		}

		// If have catalogue, fetch catalogue
		var catalogue interface{}
		if resOrder["merchant_menubook"] == 1.0 {
			rbc, _ := json.Marshal(DataMap{"table_id":id})
			resCatalogue := util.ReqPost(env.API_HARE_URL + "menubook/merchant/fetch", env.API_HARE_TOKEN, rbc)
			catalogue = resCatalogue["data"]
		}

		// Catalogue
		scatalogue := []DataMap{
			DataMap{
				"merchant_menubook_page": "1",
				"merchant_menubook_title": "Food",
				"content": []DataMap{
					DataMap{
						"menubook_content_layout": "1",
						"data": DataMap{
							"section_1": []DataMap{
								DataMap{
									"product_name": "Iga Penyet",
									"product_price": 10000,
									"product_img": "https://s3-ap-southeast-1.amazonaws.com/hare.co.id/menu/1606732375_iga-penyet.jpg",
								},
							},
							"section_2": []DataMap{
								DataMap{
									"product_name": "Nasi goreng",
									"product_price": 10000,
									"product_img": "https://s3-ap-southeast-1.amazonaws.com/hare.co.id/menu/1606732375_iga-penyet.jpg",
								},
							},
						},
					},
					DataMap{
						"menubook_content_layout": "2",
						"data": DataMap{
							"section_1": []DataMap{
								DataMap{
									"product_name": "Buntut Penyet",
									"product_price": 10000,
									"product_img": "https://cfcdn2.azsg.opensnap.com/azsg/snapphoto/photo/LD/GVYJ/3C39KXA8FDE851F378E22Elv.jpg",
								},
							},
							"section_2": []DataMap{
								DataMap{
									"product_name": "Ayam Penyet",
									"product_price": 10000,
									"product_img": "https://i.pinimg.com/474x/e9/44/45/e94445f446f0af949200ca759a5cc0ec.jpg",
								},
							},
						},
					},
					DataMap{
						"menubook_content_layout": "2",
						"data": DataMap{
							"section_1": []DataMap{
								DataMap{
									"product_name": "Nasi Goreng",
									"product_price": 10000,
									"product_img": "https://blue.kumparan.com/image/upload/w_1200,h_1200,c_fill,ar_1:1,f_jpg,q_auto/l_i8wjpgngcm1dgiyudijf,g_south,w_600/l_text:Verdana_12:User%20Story%20%7C%20Resep%20Masakan,g_south_west,x_10,y_10,co_white/asxtrr2ga1os4abfmuoe.jpg",
								},
							},
							"section_2": []DataMap{
								DataMap{
									"product_name": "Lele Sambal Ijo",
									"product_price": 10000,
									"product_img": "https://doyanresep.com/wp-content/uploads/2019/11/resep-pecel-lele.jpg",
								},
							},
						},
					},
					DataMap{
						"menubook_content_layout": "3",
						"data": DataMap{
							"section_1": []DataMap{
								DataMap{
									"menubook_data_text": "Spesial Penyetan",
									"product_name": "Ayam goreng",
									"product_price": 10000,
								},
							},
							"section_2": []DataMap{
								DataMap{
									"product_name": "Nasi goreng",
									"product_price": 10000,
									"product_img": "https://s3-ap-southeast-1.amazonaws.com/hare.co.id/menu/1606732375_iga-penyet.jpg",
								},
								DataMap{
									"product_name": "Iga Bakar",
									"product_price": 10000,
									"product_img": "https://s3-ap-southeast-1.amazonaws.com/hare.co.id/menu/1606732375_iga-penyet.jpg",
								},
								DataMap{
									"product_name": "Iga Bakar",
									"product_price": 10000,
									"product_img": "https://s3-ap-southeast-1.amazonaws.com/hare.co.id/menu/1606732375_iga-penyet.jpg",
								},
								DataMap{
									"product_name": "Iga Bakar",
									"product_price": 10000,
									"product_img": "https://s3-ap-southeast-1.amazonaws.com/hare.co.id/menu/1606732375_iga-penyet.jpg",
								},
								DataMap{
									"product_name": "Iga Bakar",
									"product_price": 10000,
									"product_img": "https://s3-ap-southeast-1.amazonaws.com/hare.co.id/menu/1606732375_iga-penyet.jpg",
								},
								DataMap{
									"product_name": "Iga Bakar",
									"product_price": 10000,
									"product_img": "https://s3-ap-southeast-1.amazonaws.com/hare.co.id/menu/1606732375_iga-penyet.jpg",
								},
								DataMap{
									"product_name": "Iga Bakar",
									"product_price": 10000,
									"product_img": "https://s3-ap-southeast-1.amazonaws.com/hare.co.id/menu/1606732375_iga-penyet.jpg",
								},
								DataMap{
									"product_name": "Iga Bakar",
									"product_price": 10000,
									"product_img": "https://s3-ap-southeast-1.amazonaws.com/hare.co.id/menu/1606732375_iga-penyet.jpg",
								},
								DataMap{
									"product_name": "Iga Bakar",
									"product_price": 10000,
									"product_img": "https://s3-ap-southeast-1.amazonaws.com/hare.co.id/menu/1606732375_iga-penyet.jpg",
								},
								DataMap{
									"product_name": "Iga Bakar",
									"product_price": 10000,
									"product_img": "https://s3-ap-southeast-1.amazonaws.com/hare.co.id/menu/1606732375_iga-penyet.jpg",
								},
								DataMap{
									"product_name": "Iga Bakar",
									"product_price": 10000,
									"product_img": "https://s3-ap-southeast-1.amazonaws.com/hare.co.id/menu/1606732375_iga-penyet.jpg",
								},
								DataMap{
									"product_name": "Iga Bakar",
									"product_price": 10000,
									"product_img": "https://s3-ap-southeast-1.amazonaws.com/hare.co.id/menu/1606732375_iga-penyet.jpg",
								},
							},
						},
					},
				},
			},
			DataMap{
				"merchant_menubook_page": "2",
				"merchant_menubook_title": "Drinks",
				"content": []DataMap{
					DataMap{
						"menubook_content_layout": "1",
						"data": DataMap{
							"section_1": []DataMap{
								DataMap{
									"product_name": "Es Teler",
									"product_price": 10000,
									"product_img": "https://s3-ap-southeast-1.amazonaws.com/hare.dev/menu/es_teler.jpg",
								},
							},
						},
					},
					DataMap{
						"menubook_content_layout": "1",
						"data": DataMap{
							"section_1": []DataMap{
								DataMap{
									"product_name": "Es Jeruk Leci",
									"product_price": 10000,
									"product_img": "https://s3-ap-southeast-1.amazonaws.com/hare.dev/menu/es_jeruk_leci.jpg",
								},
							},
						},
					},
					DataMap{
						"menubook_content_layout": "3",
						"data": DataMap{
							"section_1": []DataMap{
								DataMap{
									"menubook_data_text": "Minuman",
									"product_name": "",
									"product_price": "",
								},
							},
							"section_2": []DataMap{
								DataMap{
									"product_name": "Es Jeruk Kelapa",
									"product_price": 9500,
									"product_img": "",
								},
								DataMap{
									"product_name": "Es Jeruk Manis",
									"product_price": 13000,
									"product_img": "",
								},
								DataMap{
									"product_name": "Es Tomat Buah",
									"product_price": 11500,
									"product_img": "",
								},
								DataMap{
									"product_name": "Es Mentimun",
									"product_price": 12000,
									"product_img": "",
								},
							},
						},
					},
				},
			},
			DataMap{
				"merchant_menubook_page": "3",
				"merchant_menubook_title": "Snack",
				"content": []DataMap{
					DataMap{
						"menubook_content_layout": "2",
						"data": DataMap{
							"section_1": []DataMap{
								DataMap{
									"product_name": "Buntut Penyet",
									"product_price": 10000,
									"product_img": "https://s3-ap-southeast-1.amazonaws.com/hare.co.id/menu/1606735001_buntut-penyet.jpg",
								},
							},
							"section_2": []DataMap{
								DataMap{
									"product_name": "Ayam Penyet",
									"product_price": 10000,
									"product_img": "https://s3-ap-southeast-1.amazonaws.com/hare.co.id/menu/1606735485_ayam-penyet.jpg",
								},
							},
						},
					},
					DataMap{
						"menubook_content_layout": "1",
						"data": DataMap{
							"section_1": []DataMap{
								DataMap{
									"product_name": "Iga Penyet",
									"product_price": 10000,
									"product_img": "https://s3-ap-southeast-1.amazonaws.com/hare.co.id/menu/1606732375_iga-penyet.jpg",
								},
							},
							"section_2": []DataMap{
								DataMap{
									"product_name": "Nasi goreng",
									"product_price": 10000,
									"product_img": "https://s3-ap-southeast-1.amazonaws.com/hare.co.id/menu/1606732375_iga-penyet.jpg",
								},
							},
						},
					},
					DataMap{
						"menubook_content_layout": "2",
						"data": DataMap{
							"section_1": []DataMap{
								DataMap{
									"product_name": "Buntut Penyet",
									"product_price": 10000,
									"product_img": "https://s3-ap-southeast-1.amazonaws.com/hare.co.id/menu/1606735001_buntut-penyet.jpg",
								},
							},
							"section_2": []DataMap{
								DataMap{
									"product_name": "Ayam Penyet",
									"product_price": 10000,
									"product_img": "https://s3-ap-southeast-1.amazonaws.com/hare.co.id/menu/1606735485_ayam-penyet.jpg",
								},
							},
						},
					},
					DataMap{
						"menubook_content_layout": "3",
						"data": DataMap{
							"section_1": []DataMap{
								DataMap{
									"menubook_data_text": "Spesial Penyetan",
									"product_name": "Ayam goreng",
									"product_price": 10000,
								},
							},
							"section_2": []DataMap{
								DataMap{
									"product_name": "Nasi goreng",
									"product_price": 10000,
									"product_img": "https://s3-ap-southeast-1.amazonaws.com/hare.co.id/menu/1606732375_iga-penyet.jpg",
								},
								DataMap{
									"product_name": "Iga Bakar",
									"product_price": 10000,
									"product_img": "https://s3-ap-southeast-1.amazonaws.com/hare.co.id/menu/1606732375_iga-penyet.jpg",
								},
								DataMap{
									"product_name": "Iga Bakar",
									"product_price": 10000,
									"product_img": "https://s3-ap-southeast-1.amazonaws.com/hare.co.id/menu/1606732375_iga-penyet.jpg",
								},
								DataMap{
									"product_name": "Iga Bakar",
									"product_price": 10000,
									"product_img": "https://s3-ap-southeast-1.amazonaws.com/hare.co.id/menu/1606732375_iga-penyet.jpg",
								},
								DataMap{
									"product_name": "Iga Bakar",
									"product_price": 10000,
									"product_img": "https://s3-ap-southeast-1.amazonaws.com/hare.co.id/menu/1606732375_iga-penyet.jpg",
								},
								DataMap{
									"product_name": "Iga Bakar",
									"product_price": 10000,
									"product_img": "https://s3-ap-southeast-1.amazonaws.com/hare.co.id/menu/1606732375_iga-penyet.jpg",
								},
								DataMap{
									"product_name": "Iga Bakar",
									"product_price": 10000,
									"product_img": "https://s3-ap-southeast-1.amazonaws.com/hare.co.id/menu/1606732375_iga-penyet.jpg",
								},
							},
						},
					},
				},
			},
		}
		log.Println(scatalogue)

		c.HTML(200, "menu", gin.H{
			"DataMenu": resDataMenu,
			"DataOrder": resMap["data_order"],
			"tid": util.Encrypt(id, constants.AesKeyTable),
			"S3": env.URL_S3,
			"Cdn": env.URL_CDN,
			"G": global.G,
			"Order_finish": order_finish,
			"Order_total": order_total,
			"Table_auth": table_auth,
			"P": table_pass,
			"Notif": c.Request.URL.Query().Get("notif"),
			"Catalogue": catalogue,
		})
	} else {
		c.HTML(200, "menu", nil)
	}
}

func ShowV1(c *gin.Context) {
	id := c.Param("id")
	order_finish := c.Request.URL.Query().Get("order")

	res := util.SendReqGet(env.API_HARE_URL + "table/" + id, env.API_HARE_TOKEN)

	//log.Println(util.AddTS("1211234567"))

	var resMap DataMap
	json.Unmarshal([]byte(res), &resMap)
	resStatus := int(resMap["status"].(float64))
	
	if(resStatus == 200) {
		resData := resMap["data"]

		// Count total
		var order_total float64
		order_total = 0

		resOrder := resMap["data"].(map[string]interface{})
		arrOrder := resOrder["order"].([]interface{})
		// log.Println(resMap["data"]);
		log.Println(arrOrder);

		for _, amt := range arrOrder {
			// od, ok := rec.(map[string]interface{})
			//log.Println("YYYYYYYYYYYYYYYYYYY" + strconv.amt.(map[string]interface{})["order_id"].(float64))

			objOrder := amt.(map[string]interface{})
			arrMenu := objOrder["menu"].([]interface{})

			for _, men := range arrMenu {
				// log.Println("YYYYYYYYYY" + men.(map[string]interface{})["menu_name"].(string))
				objMenu := men.(map[string]interface{})
				mqty := objMenu["qty"].(float64)
				mprice := objMenu["order_menu_price"].(float64)
				mtotal := mqty * mprice
				order_total += mtotal
			}
		}

		log.Println(order_total)

		c.HTML(200, "menu", gin.H{
			"data": resData,
			"tid": id,
			"S3": env.URL_S3,
			"Cdn": env.URL_CDN,
			"Order_finish": order_finish,
			"Order_total": order_total,
		})
	} else {
		c.HTML(200, "menu", nil)
	}
}

func ShowDetail(c *gin.Context) {
	id := c.Param("id")
	qty := c.PostForm("qty")
	unavailable := c.PostForm("unavailable")
	mtype := c.PostForm("mtype")

	res := util.SendReqGet(env.API_HARE_URL + "menu/" + id, env.API_HARE_TOKEN)

	var resMap DataMap
	json.Unmarshal([]byte(res), &resMap)
	resStatus := int(resMap["status"].(float64))

	if string(qty) == "0" {
		qty = "1"
	}
	
	if resStatus == 200 {
		resData := resMap["data"]
		c.HTML(200, "menu_detail", gin.H{
			"data": resData,
			"S3": env.URL_S3,
			"Qty": qty,
			"G": global.G,
			"Unavailable": unavailable,
			"Package_type": mtype,
		})
	} else {
		c.HTML(200, "menu_detail", nil)
	}
}

func ShowDetailLoader(c *gin.Context) {
	c.HTML(200, "menu_detail_loader", nil)
}

func SubmitOrderNew(c *gin.Context) {
	order_id := c.PostForm("order_id")
	table_id := util.Decrypt(c.PostForm("table_id"), constants.AesKeyTable)
	order := c.PostForm("order")

	var orderMap DataMap
	json.Unmarshal([]byte(order), &orderMap)

	if order_id == "" {
		// New
		rb, err := json.Marshal(DataMap{
			"table_id": table_id,
			"order_payment_type": 0,
			"order_payment_amount": 0,
			"order_payment_id": 0,
			"order": orderMap["data"],
			"order_id": "",
		})
		if err != nil { log.Println(err) }

		res := util.SendReqPost(env.API_HARE_URL + "order/store", env.API_HARE_TOKEN, rb)
		var resMap DataMap
		json.Unmarshal([]byte(res), &resMap)

		c.JSON(200, gin.H{
			"status": resMap["status"],
			"oid": resMap["order_id"],
		})
	} else {
		// Update
		rb, err := json.Marshal(DataMap{
			"order_id": order_id,
			"order": orderMap["data"],
		})
		if err != nil { log.Println(err) }

		res := util.SendReqPost(env.API_HARE_URL + "order/store", env.API_HARE_TOKEN, rb)
		var resMap DataMap
		json.Unmarshal([]byte(res), &resMap)

		c.JSON(200, gin.H{
			"status": resMap["status"],
			"oid": resMap["order_id"],
		})
	}
}

func HelpSubmit(c *gin.Context) {
	tid := util.Decrypt(c.PostForm("tid"), constants.AesKeyTable)	
	msg := c.PostForm("msg")

	rb, _ := json.Marshal(DataMap{
		"table_id": tid,
		"table_bell_notes": msg,
	})
	res := util.ReqPost(env.API_HARE_URL + "tablebell/store", env.API_HARE_TOKEN, rb)
	
	c.JSON(200, gin.H{
		"Status": res["status"],
	})
}

func BillLoad(c *gin.Context) {
	tid := util.Decrypt(c.PostForm("tid"), constants.AesKeyTable)

	rb, _ := json.Marshal(DataMap{ "table_id": tid, })
	res := util.ReqPost(env.API_HARE_URL + "order/menu/table", env.API_HARE_TOKEN, rb)

	total := 0.0
	if res["status"] == 200.0 {
		resData := res["data"].([]interface{})
		if len(resData) > 0 {
			resData := res["data"].([]interface{})
			resDataFirst := resData[0].(map[string]interface{})
			resMenu := resDataFirst["menus"].([]interface{})

			for _, menu := range resMenu {
				menuData := menu.(map[string]interface{})
				total += menuData["qty"].(float64) * menuData["order_menu_price"].(float64)
			}
		}
	}

	c.HTML(200, "menu_bill_load", gin.H {
		"Data": res["data"],
		"Total": total,
		"G": global.G,
	})
}