package corder

import (
	"encoding/json"
	"log"

	"utils"
	"hare/envi"

	"github.com/gin-gonic/gin"
)

var env = envi.Config
type DataMap map[string]interface{}

func Done(c *gin.Context) {
	order_id := c.PostForm("order_id")
	
	rb, err := json.Marshal(DataMap{
		"order_id": order_id,
		"status": 3,
	})
	if err != nil { log.Println(err) }

	res := util.ReqPut(env.API_HARE_URL + "order/update", env.API_HARE_TOKEN, rb)

	c.JSON(200, gin.H{
		"status": res["status"],
	})
}