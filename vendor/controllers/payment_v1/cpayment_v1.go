package cpayment

import (
	"log"
	"net/http"
	"encoding/json"
	"strconv"
	"time"

	// "hare/utils"
	"hare/constants"
	
	"github.com/gin-gonic/gin"
	midtrans "github.com/veritrans/go-midtrans"

)

var env = constants.Env;
var tmpl = constants.TmplRoot
var tmpl_site = constants.TmplSite
var tmpl_common = constants.TmplCommon
type DataMap map[string]interface{}

var midclient midtrans.Client
var coreGateway midtrans.CoreGateway
var snapGateway midtrans.SnapGateway

func Checkout(c *gin.Context) {

	setupMidtrans()
	new_id := generateOrderID()
	log.Println(new_id)
	snapResp, err := snapGateway.GetTokenQuick(new_id, 200000)
	if err != nil { log.Println(err) }

	// var paymentToken DataMap
	// if err != nil {
	// 	paymentToken["Token"] = ""
	// } else {
	// 	paymentToken["Token"] = snapResp.Token
	// }

	var dataTest = make(map[string]interface{})
	dataTest["ClientKey"] = midclient.ClientKey

	c.HTML(200, "checkout", gin.H{
		"ClientKey": midclient.ClientKey,
		"Token": snapResp.Token,
	})
}

func setupMidtrans() {
	midclient = midtrans.NewClient()
	midclient.ServerKey = "SB-Mid-server-nBZb8CKN-bj9oxKyLIfxJfe0"
	midclient.ClientKey = "SB-Mid-client-vS9snK-GSZbcZpBN"
	midclient.APIEnvType = midtrans.Sandbox

	coreGateway = midtrans.CoreGateway{
		Client: midclient,
	}

	snapGateway = midtrans.SnapGateway{
		Client: midclient,
	}
}

func chargeDirect(w http.ResponseWriter, r *http.Request) {
	chargeResp, _ := coreGateway.Charge(&midtrans.ChargeReq{
		PaymentType: midtrans.SourceCreditCard,
		CreditCard: &midtrans.CreditCardDetail{
			TokenID: r.FormValue("card-token"),
		},
		TransactionDetails: midtrans.TransactionDetails{
			OrderID:  generateOrderID(),
			GrossAmt: 200000,
		},
	})

	result, err := json.Marshal(chargeResp)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	w.Header().Set("Content-Type", "application/json")
	w.Write(result)
}

func ChargeWithMap(w http.ResponseWriter, r *http.Request) {
	var reqPayload = &midtrans.ChargeReqWithMap{}
	err := json.NewDecoder(r.Body).Decode(reqPayload)
	if err != nil {
		response := make(map[string]interface{})
		response["status_code"] = 400
		response["status_message"] = "please fill request payload, refer to https://api-docs.midtrans.com depend on payment method"

		w.Header().Set("Content-Type", "application/json")
		w.WriteHeader(http.StatusBadRequest)
		json.NewEncoder(w).Encode(response)
		return
	}

	chargeResp, _ := coreGateway.ChargeWithMap(reqPayload)
	result, err := json.Marshal(chargeResp)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	w.Header().Set("Content-Type", "application/json")
	w.Write(result)
}

func notification(w http.ResponseWriter, r *http.Request) {
	var reqPayload = &midtrans.ChargeReqWithMap{}
	err := json.NewDecoder(r.Body).Decode(reqPayload)
	if err != nil {
		response := make(map[string]interface{})
		response["status_code"] = 400
		response["status_message"] = "please fill request payload, refer to https://api-docs.midtrans.com/#receiving-notifications"

		w.Header().Set("Content-Type", "application/json")
		w.WriteHeader(http.StatusBadRequest)
		json.NewEncoder(w).Encode(response)
		return
	}

	encode, _ := json.Marshal(reqPayload)
	resArray := make(map[string]string)
	err = json.Unmarshal(encode, &resArray)

	chargeResp, _ := coreGateway.StatusWithMap(resArray["order_id"])
	result, err := json.Marshal(chargeResp)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	w.Header().Set("Content-Type", "application/json")
	w.Write(result)
}


func generateOrderID() string {
	return strconv.FormatInt(time.Now().UnixNano(), 10)
}