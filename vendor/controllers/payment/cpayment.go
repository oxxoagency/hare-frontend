package cpayment

import (
	"log"
	"fmt"
	"encoding/json"
	// "net/http"
	"strconv"
	"time"
	"io/ioutil"

	"utils"
	"constants"
	"hare/envi"
	
	"github.com/gin-gonic/gin"
	midtrans "github.com/veritrans/go-midtrans"
	"github.com/xendit/xendit-go"
	"github.com/xendit/xendit-go/invoice"
)

var env = envi.Config;
var tmpl = constants.TmplRoot
var tmpl_site = constants.TmplSite
var tmpl_common = constants.TmplCommon
type DataMap map[string]interface{}

var midclient midtrans.Client
var coreGateway midtrans.CoreGateway
var snapGateway midtrans.SnapGateway

func Process(c *gin.Context) {
	// table_id := c.PostForm("table_id")
	pay := c.PostForm("pay")
	amt := c.PostForm("amt")
	order_id := c.PostForm("order_id")

	var payment_id string
	var payment_ref string

	// Check payment choice
	if pay == "1" {
		log.Println(amt)
		zamount, _ := strconv.ParseFloat(amt, 64)
		log.Println(zamount)

		// Ovo
		xendit.Opt.SecretKey = env.XENDIT_KEY

		extID := generateOrderID()
		xdata := invoice.CreateParams{
			ExternalID: extID,
			Amount: zamount,
			PayerEmail:  "customer@customer.com",
			Description: "invoice #1",
			SuccessRedirectURL: "https://hare.co.id/payment/finish?pid=" + extID,
			FailureRedirectURL: "https://hare.co.id/payment/failed?pid=" + extID,
		}

		resp, err := invoice.Create(&xdata)
		if err != nil {
			log.Fatal(err)
		}

		fmt.Printf("created invoice: %+v\n", resp)
		fmt.Printf("created invoice: %+v\n", resp.ID)

		payment_id = extID
		payment_ref = resp.ID
	} else if pay == "2" {
		// Gopay
		payment_id = generateOrderID()
	}

	rb, err := json.Marshal(DataMap{
		"order_id": order_id,
		"status": 0,
		"order_payment_type": pay,
		"order_payment_amount": amt,
		"order_payment_id": payment_id,
		"order_payment_ref": payment_ref,
	})
	if err != nil { log.Println(err) }
	log.Println(rb)
	
	res := util.ReqPut(env.API_HARE_URL + "order/update", env.API_HARE_TOKEN, rb)
	resStatus := int(res["status"].(float64))

	if resStatus == 200 {
		amt_int, err := strconv.Atoi(amt)
		if err != nil { log.Println(err) }

		var pay_method string
		switch pay {
			case "1":
				pay_method = "OVO"
			case "2":
				pay_method = "GoPay"
			case "3":
				pay_method = "Bayar di Kasir"
		}

		tns := 0
		additional := 0
		total_amt := amt_int + tns + additional

		payment_amt := int64(total_amt)
		if err != nil { log.Println(err) }

		// Gopay Token
		var snapToken string
		if pay == "2" {
			setupMidtrans()
			snapResp, err := snapGateway.GetTokenQuick(payment_id, payment_amt)
			if err != nil { log.Println(err) }
			snapToken = snapResp.Token
		}

		// var dataPay = make(map[string]interface{})
		// dataPay["ClientKey"] = midclient.ClientKey

		c.HTML(200, "checkout_btn", gin.H{
			// "ClientKey": midclient.ClientKey,
			"Token": snapToken,
			"Base": amt,
			"Tns": tns,
			"Additional": additional,
			"Total": total_amt,
			"Payment_id": payment_id,
			"Payment_ref": payment_ref,
			"Payment_method": pay_method,
			"Pay_code": pay,
			"Order_id": order_id,
			// "Order": resMap["order"],
		})
	} else {
		log.Println("FAILED")
	}
}

func ProcessV1(c *gin.Context) {
	table_id := c.PostForm("table_id")
	pay := c.PostForm("pay")
	amt := c.PostForm("amt")
	order := c.PostForm("order")

	var payment_id string

	// Check payment choice
	if pay == "1" {
		log.Println(amt)
		zamount, _ := strconv.ParseFloat(amt, 64)
		log.Println(zamount)

		// Ovo
		xendit.Opt.SecretKey = env.XENDIT_KEY

		extID := strconv.FormatInt(time.Now().Unix(), 10)
		xdata := invoice.CreateParams{
			ExternalID: extID,
			Amount: zamount,
			PayerEmail:  "customer@customer.com",
			Description: "invoice #1",
		}

		resp, err := invoice.Create(&xdata)
		if err != nil {
			log.Fatal(err)
		}

		fmt.Printf("created invoice: %+v\n", resp)
		fmt.Printf("created invoice: %+v\n", resp.ID)

		payment_id = resp.ID
	} else if pay == "2" {
		// Gopay
		payment_id = generateOrderID()
	}

	// Get order menu
	var orderMap DataMap
	json.Unmarshal([]byte(order), &orderMap)
	log.Println(orderMap["data"])

	rb, err := json.Marshal(DataMap{
		"table_id": table_id,
		"order_payment_type": pay,
		"order_payment_amount": amt,
		"order_payment_id": payment_id,
		"order": orderMap["data"],
	})
	if err != nil { log.Println(err) }
	log.Println(rb)
	
	res := util.SendReqPost(env.API_HARE_URL + "order/store", env.API_HARE_TOKEN, rb)
	var resMap DataMap
	json.Unmarshal([]byte(res), &resMap)
	resStatus := int(resMap["status"].(float64))

	if resStatus == 200 {
		amt_int, err := strconv.Atoi(amt)
		if err != nil { log.Println(err) }

		var pay_method string
		switch pay {
			case "1":
				pay_method = "OVO"
			case "2":
				pay_method = "GoPay"
			case "3":
				pay_method = "Bayar di Kasir"
		}

		tns := 0
		additional := 0
		total_amt := amt_int + tns + additional

		payment_amt := int64(total_amt)
		if err != nil { log.Println(err) }

		// Gopay Token
		var snapToken string
		if pay == "2" {
			setupMidtrans()
			snapResp, err := snapGateway.GetTokenQuick(payment_id, payment_amt)
			if err != nil { log.Println(err) }
			snapToken = snapResp.Token
		}

		// var dataPay = make(map[string]interface{})
		// dataPay["ClientKey"] = midclient.ClientKey

		c.HTML(200, "checkout_btn", gin.H{
			// "ClientKey": midclient.ClientKey,
			"Token": snapToken,
			"Base": amt,
			"Tns": tns,
			"Additional": additional,
			"Total": total_amt,
			"Payment_id": payment_id,
			"Payment_method": pay_method,
			"Pay_code": pay,
			"Order": resMap["order"],
		})
	} else {
		log.Println("FAILED")
	}
}

func Success(c *gin.Context) {
	pid := c.Request.URL.Query().Get("pid")

	rb, err := json.Marshal(DataMap{
		"order_payment_id": pid,
		"status": "2",
	})
	if err != nil { log.Println(err) }

	res := util.SendReqPut(env.API_HARE_URL + "order/updatepayment", env.API_HARE_TOKEN, rb)
	var resMap DataMap
	json.Unmarshal([]byte(res), &resMap)

	c.HTML(200, "payment_success", gin.H{
		"S3": env.URL_S3,
		"Status": resMap["status"],
		"Asset": env.URL_ASSET,
		"Cdn": env.URL_CDN,
	})
}

func Failed(c *gin.Context) {
	c.HTML(200, "payment_failed", nil)
}

func Pending(c *gin.Context) {
	c.HTML(200, "payment_failed", nil)
}

func Notification(c *gin.Context) {
	
}

func XenditInvoiceFinish(c *gin.Context) {
	jsonData := c.Request.Body
	body, err := ioutil.ReadAll(jsonData)
	if err != nil { log.Println(err) }
	var jsonMap DataMap
	json.Unmarshal([]byte(body), &jsonMap)
	log.Println(jsonMap)

	rb, err := json.Marshal(DataMap{
		"id": jsonMap["id"],
		"external_id": jsonMap["external_id"],
		"user_id": jsonMap["user_id"],
		"is_high": jsonMap["is_high"],
		"payment_method": jsonMap["payment_method"],
		"status": jsonMap["status"],
		"merchant_name": jsonMap["merchant_name"],
		"amount": jsonMap["amount"],
		"paid_amount": jsonMap["paid_amount"],
		"bank_code": jsonMap["bank_code"],
		"paid_at": jsonMap["paid_at"],
		"payer_email": jsonMap["payer_email"],
		"description": jsonMap["description"],
		"adjusted_received_amount": jsonMap["adjusted_received_amount"],
		"fees_paid_amount": jsonMap["fees_paid_amount"],
		"updated": jsonMap["updated"],
		"created": jsonMap["created"],
		"payment_channel": jsonMap["payment_channel"],
		"payment_destination": jsonMap["payment_destination"],
	})
	if err != nil { log.Println(err) }

	res := util.ReqPost(env.API_HARE_URL + "paymentlog/xendit", env.API_HARE_TOKEN, rb)

	// Change order status
	rbs, err := json.Marshal(DataMap{
		"order_payment_id": jsonMap["external_id"],
		"status": "2",
	})
	if err != nil { log.Println(err) }

	resOrder := util.ReqPut(env.API_HARE_URL + "order/updatepayment", env.API_HARE_TOKEN, rbs)

	c.JSON(200, gin.H{
		"status": res["status"],
		"order": resOrder["status"],
	})
}

func GopayNotification(c *gin.Context) {
	jsonData := c.Request.Body
	body, err := ioutil.ReadAll(jsonData)
	if err != nil { log.Println(err) }
	var jsonMap DataMap
	json.Unmarshal([]byte(body), &jsonMap)
	log.Println(jsonMap)

	rb, err := json.Marshal(DataMap{
		"transaction_time": jsonMap["transaction_time"],
		"transaction_status": jsonMap["transaction_status"],
		"transaction_id": jsonMap["transaction_id"],
		"status_message": jsonMap["status_message"],
		"status_code": jsonMap["status_code"],
		"signature_key": jsonMap["signature_key"],
		"settlement_time": jsonMap["settlement_time"],
		"payment_type": jsonMap["payment_type"],
		"order_id": jsonMap["order_id"],
		"merchant_id": jsonMap["merchant_id"],
		"gross_amount": jsonMap["gross_amount"],
		"fraud_status": jsonMap["fraud_status"],
		"currency": jsonMap["currency"],
	})
	if err != nil { log.Println(err) }

	res := util.SendReqPost(env.API_HARE_URL + "paymentlog/gopay", env.API_HARE_TOKEN, rb)
	var resMap DataMap
	json.Unmarshal([]byte(res), &resMap)

	c.JSON(200, gin.H{
		"status": resMap["status"],
	})
}

func setupMidtrans() {
	midclient = midtrans.NewClient()
	midclient.ServerKey = env.MIDTRANS_SERVER
	midclient.ClientKey = env.MIDTRANS_CLIENT
	midclient.APIEnvType = midtrans.Sandbox

	coreGateway = midtrans.CoreGateway{
		Client: midclient,
	}

	snapGateway = midtrans.SnapGateway{
		Client: midclient,
	}
}

func generateOrderID() string {
	return strconv.FormatInt(time.Now().UnixNano(), 10)
}

func SendReceipt(c *gin.Context) {
	email := c.PostForm("email")
	opid := c.PostForm("opid")

	rb, err := json.Marshal(DataMap{
		"user_email": email,
		"order_payment_id": opid,
	})
	if err != nil { log.Println(err) }

	res := util.ReqPost(env.API_HARE_URL + "receipt/mail", env.API_HARE_TOKEN, rb)
	
	c.JSON(200, gin.H{
		"status": res["status"],
	})

}