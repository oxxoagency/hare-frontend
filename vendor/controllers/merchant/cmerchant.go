package cmerchant

import (
	"log"
	// "net/http"
	// "html/template"
	"encoding/json"
	"encoding/base64"
	// "bufio"
	"io/ioutil"

	"utils"
	"constants"
	"hare/envi"
	"global"
	
	"github.com/gin-gonic/gin"
)

var env = envi.Config
var tmpl = constants.TmplRoot
var tmpl_site = constants.TmplSite
var tmpl_common = constants.TmplCommon
type DataMap map[string]interface{}
type DM map[string]interface{}

func Dashboard(c *gin.Context) {
	//mid := c.Param("id")
	authData := util.GetSession(c)

	if authData.AuthM.(bool) {
		mid := authData.Mid

		// Dashboard menu
		menu := []DM{
			DM{"t":"Menu", "u":"/merchant/menu", "i":"menubook2"},
			DM{"t":"Catalogue", "u":"/merchant/catalogue", "i":"menubook2"},
			DM{"t":"Outlet", "u":"/merchant/branch", "i":"outlet"},
			DM{"t":"Settings", "u":"/merchant/settings", "i":"settings2"},
		}

		res := util.ReqGet(env.API_HARE_URL + "merchant/show/" + mid.(string), env.API_HARE_TOKEN)

		c.HTML(200, "merchant_dashboard", gin.H{
			"mid": mid,
			"G": global.G,
			"Data": res["data"],
			"Page": "home",
			"Menu": menu,
		})
	} else {
		c.Redirect(302, "/login")
	}
}

func New(c *gin.Context) {
	c.HTML(200, "merchant_new", nil)
}

func SubmitNew(c *gin.Context) {
	var authData = util.GetSession(c)
	uid := authData.Uid.(string)

	merchant_name := c.PostForm("merchant_name")
	branch_name := c.PostForm("branch_name")
	branch_phone := c.PostForm("branch_phone")
	branch_address := c.PostForm("branch_address")

	rb, err := json.Marshal(DataMap{
		"user_id": uid,
		"merchant_name": merchant_name,
		"branch_name": branch_name,
		"branch_phone": branch_phone,
		"branch_address": branch_address,
		"merchant_logo": "1.jpg",
	})
	if err != nil { log.Println(err) }

	res := util.SendReqPost(env.API_HARE_URL + "merchant/store", env.API_HARE_TOKEN, rb)
	var resMap DataMap
	json.Unmarshal([]byte(res), &resMap)
	
	c.JSON(200, gin.H{
		"status": resMap["status"],
		"data": resMap["data"],
	})
}

func Menu(c *gin.Context) {
	// mid := c.Param("id")
	authData := util.GetSession(c)
	log.Println(authData)

	if authData.AuthM.(bool) {
		paramSearch := c.Request.URL.Query().Get("s")
		paramSid := c.Request.URL.Query().Get("sid")
		paramOrder := c.Request.URL.Query().Get("order")
		paramSort := c.Request.URL.Query().Get("sort")
		mid := authData.Mid

		// Get data menu
		rb, err := json.Marshal(DataMap{
			"merchant_id": mid,
			"limit": 20,
			"page": 1,
			"query": paramSearch,
			"menu_section_id": paramSid,
			"order_by": paramOrder,
			"sort": paramSort,
		})
		if err != nil { log.Println(err) }

		res := util.ReqPost(env.API_HARE_URL + "menu/merchant", env.API_HARE_TOKEN, rb)

		rbs, _ := json.Marshal(DataMap{
			"merchant_id": mid,
			"limit": 50,
			"page": 1,
			"query": "",
		})
		resSection := util.ReqPost(env.API_HARE_URL + "section/merchant", env.API_HARE_TOKEN, rbs)

		c.HTML(200, "merchant_menu", gin.H{
			"data": res["data"],
			"Section": resSection,
			"mid": mid,
			"G": global.G,
			"auth": authData,
			"Page": "menu",
			"ParamSearch": paramSearch,
			"ParamSid": paramSid,
			"ParamOrder": paramOrder,
			"ParamSort": paramSort,
		})
	} else {
		c.Redirect(302, "/login")
	}
}

func MenuSearch(c *gin.Context) {
	authData := util.GetSession(c)
	mid := authData.Mid
	search := c.PostForm("search")
	sid := c.PostForm("sid")
	order := c.PostForm("order")
	sort := c.PostForm("sort")

	rb, _ := json.Marshal(DataMap{
		"merchant_id": mid,
		"limit": 25,
		"page": 1,
		"query": search,
		"menu_section_id": sid,
		"order_by": order,
		"sort": sort,
	})
	res := util.ReqPost(env.API_HARE_URL + "menu/merchant", env.API_HARE_TOKEN, rb)

	c.HTML(200, "merchant_menu_data", gin.H{
		"data": res["data"],
		"G": global.G,
	})
}

func MenuDetail(c *gin.Context) {
	authData := util.GetSession(c)

	if authData.AuthM.(bool) {
		mid := authData.Mid
		//mid := c.Param("id")
		paramSearch := c.Request.URL.Query().Get("s")
		paramSid := c.Request.URL.Query().Get("sid")
		paramOrder := c.Request.URL.Query().Get("order")
		paramSort := c.Request.URL.Query().Get("sort")

		menuid := c.Param("menuid")

		res := util.ReqGet(env.API_HARE_URL + "menu/" + menuid, env.API_HARE_TOKEN)

		c.HTML(200, "merchant_menu_detail", gin.H{
			"data": res["data"],
			"mid": mid,
			"G": global.G,
			"ParamSearch": paramSearch,
			"ParamSid": paramSid,
			"ParamOrder": paramOrder,
			"ParamSort": paramSort,
		})
	} else {
		c.Redirect(302, "/login")
	}
}

func MenuAdd(c *gin.Context) {
	authData := util.GetSession(c)

	if authData.AuthM.(bool) {
		paramSearch := c.Request.URL.Query().Get("s")
		paramSid := c.Request.URL.Query().Get("sid")
		paramOrder := c.Request.URL.Query().Get("order")
		paramSort := c.Request.URL.Query().Get("sort")
		
		//mid := c.Param("id")
		mid := authData.Mid
		c.HTML(200, "merchant_menu_add", gin.H{
			"mid": mid,
			"G": global.G,
			"ParamSearch": paramSearch,
			"ParamSid": paramSid,
			"ParamOrder": paramOrder,
			"ParamSort": paramSort,
		})
	} else {
		c.Redirect(302, "/login")
	}
}

func MenuAddSubmit(c *gin.Context) {
	menu_name := c.PostForm("menu_name")
	menu_desc := c.PostForm("menu_desc")
	menu_price := c.PostForm("menu_price")
	menu_img := c.PostForm("menu_img")
	menu_section := c.PostForm("menu_section")

	rb, err := json.Marshal(DataMap{
		"menu_name": menu_name,
		"menu_price": menu_price,
		"menu_desc": menu_desc,
		"menu_img": menu_img,
		"menu_section_id": menu_section,
		"menu_sku": c.PostForm("menu_sku"),
	})
	if err != nil { log.Println(err) }

	res := util.SendReqPost(env.API_HARE_URL + "menu/store", env.API_HARE_TOKEN, rb)
	var resMap DataMap
	json.Unmarshal([]byte(res), &resMap)
	resStatus := resMap["status"]

	c.JSON(200, gin.H{
		"status": resStatus,
	})
}

func MenuEditSubmit(c *gin.Context) {
	rb, err := json.Marshal(DataMap{
		"menu_id": c.PostForm("menu_id"),
		"menu_name": c.PostForm("menu_name"),
		"menu_price": c.PostForm("menu_price"),
		"menu_desc": c.PostForm("menu_desc"),
		"menu_section_id": c.PostForm("menu_section"),
		"menu_sku": c.PostForm("menu_sku"),
	})
	if err != nil { log.Println(err) }

	res := util.ReqPut(env.API_HARE_URL + "menu/update", env.API_HARE_TOKEN, rb)

	// Check for new img
	menu_img := c.PostForm("menu_img")
	statusImg := 0.0
	if menu_img != "" {
		// Insert new img
		rbi, _ := json.Marshal(DM{
			"menu_id": c.PostForm("menu_id"),
			"menu_img": c.PostForm("menu_img"),
		})
		resI := util.ReqPost(env.API_HARE_URL + "menu/image/store", env.API_HARE_TOKEN, rbi)
		statusImg = resI["status"].(float64)
	}

	// Check for delete img
	menu_img_del := c.PostForm("img_delete")
	statusImgDel := 0.0
	if menu_img_del != "" {
		// Delete img
		rbd, _ := json.Marshal(DM{ "menu_img_id": menu_img_del })
		resD := util.ReqPost(env.API_HARE_URL + "menu/image/delete/in", env.API_HARE_TOKEN, rbd)
		statusImgDel = resD["status"].(float64)
	}

	c.JSON(200, gin.H{
		"status": res["status"],
		"StatusImg": statusImg,
		"StatusDel": statusImgDel,
	})
}

func SectionNew(c *gin.Context) {
	section := c.PostForm("section")
	mid := c.PostForm("mid")

	rb, err := json.Marshal(DataMap{
		"merchant_id": mid,
		"menu_section_name": section,
	})
	if err != nil { log.Println(err) }

	res := util.ReqPost(env.API_HARE_URL + "section/store", env.API_HARE_TOKEN, rb)

	c.JSON(200, gin.H{
		"status": res["status"],
	})
}

func Branch(c *gin.Context) {
	// mid := c.Param("id")

	authData := util.GetSession(c)

	if authData.AuthM.(bool) {
		mid := authData.Mid

		rb, err := json.Marshal(DataMap{
			"merchant_id": mid,
			"limit": 10,
			"page": 1,
			"query": "",
		})
		if err != nil { log.Println(err) }

		res := util.SendReqPost(env.API_HARE_URL + "branch/show", env.API_HARE_TOKEN, rb)
		var resMap DataMap
		json.Unmarshal([]byte(res), &resMap)
		// resStatus := resMap["status"]
		resData := resMap["data"]

		c.HTML(200, "merchant_branch", gin.H{
			"auth": authData,
			"data": resData,
			"mid": mid,
			"G": global.G,
			"Page": "outlet",
			"Merchant": resMap["merchant"],
		})
	} else {
		c.Redirect(302, "/login")
	}
}

func BranchNew(c *gin.Context) {
	authData := util.GetSession(c)

	if authData.AuthM.(bool) {
		mid := authData.Mid

		rb, err := json.Marshal(DataMap{
			"merchant_id": mid,
			"limit": 1000,
			"page": 1,
			"query": "",
		})
		if err != nil { log.Println(err) }

		res := util.ReqPost(env.API_HARE_URL + "branch/show", env.API_HARE_TOKEN, rb)
		resM := res["merchant"].(map[string]interface{})
		quota := int(resM["merchant_quota"].(float64))
		resData := res["data"].(map[string]interface{})
		resB := resData["data"].([]interface{})
		count := len(resB)

		c.HTML(200, "merchant_branch_new", gin.H{
			"G": global.G,
			"Quota": quota,
			"Count": count,
		})
	} else {
		c.Redirect(302, "/login")
	}
}

func BranchSubmitNew(c *gin.Context) {
	authData := util.GetSession(c)

	if authData.AuthM.(bool) {
		mid := authData.Mid

		rb, _ := json.Marshal(DataMap{
			"merchant_id": mid,
			"branch_name": c.PostForm("name"),
			"branch_address": c.PostForm("address"),
			"branch_phone": c.PostForm("phone"),
			"branch_username": c.PostForm("username"),
			"branch_password": c.PostForm("password"),
		})

		res := util.ReqPost(env.API_HARE_URL + "branch/store", env.API_HARE_TOKEN, rb)
		c.JSON(200, gin.H{
			"status": res["status"],
		})
	} else {
		c.JSON(200, gin.H{
			"status": 404,
		})
	}
}

func BranchDetail(c *gin.Context) {
	authData := util.GetSession(c)
	bid := authData.Bid.(string)

	res := util.ReqGet(env.API_HARE_URL + "branch/show/" + bid, env.API_HARE_TOKEN)

	c.HTML(200, "merchant_branch_detail", gin.H{
		"data": res["data"],
	})
}

func UploadPhoto(c *gin.Context) {
	file, err := c.FormFile("file")
	filedata, err := file.Open()
	defer filedata.Close()
	if err != nil { log.Println(err) }
	
	// reader := bufio.NewReader(file)
	content, err := ioutil.ReadAll(filedata)
	if err != nil { log.Println(err) }

	encoded := base64.StdEncoding.EncodeToString(content)
	// log.Println(encoded)

	param := ""
	if c.PostForm("method") == "hare_merchant_banner" {
		param = c.PostForm("mid")
	}

	rb, err := json.Marshal(DataMap{
		"appId": env.API_UPLOAD_ID,
		"appToken": env.API_UPLOAD_TOKEN,
		"method": c.PostForm("method"),
		"body": DataMap{
			"img": encoded,
			"img_name": file.Filename,
			"param": param,
		},
	})
	// log.Println(file.Filename)
	log.Println("METHOD" + c.PostForm("method"))

	res := util.SendReqPost(env.API_UPLOAD_URL + "upload", "", rb)
	var resMap DataMap
	json.Unmarshal([]byte(res), &resMap)
	log.Println(resMap)

	// If banner upload to db
	if c.PostForm("method") == "hare_merchant_banner" {
		resData := resMap["data"].(map[string]interface{})
		rb, _ := json.Marshal(DataMap{
			"merchant_id": c.PostForm("mid"),
			"merchant_banner": resData["filename"],
		})
		resDb := util.ReqPost(env.API_HARE_URL + "merchant/banner/store", env.API_HARE_TOKEN, rb)
		log.Println(resDb)
	}

	c.JSON(200, gin.H{
		"status": resMap["status"],
		"data": resMap["data"],
	})
}

func NavigateBranch(c *gin.Context) {
	bid := c.PostForm("bid")

	util.SetSessionBranch(c, true, bid)

	c.JSON(200, gin.H{
		"status": 200,
	})
}

func Settings(c *gin.Context) {
	authData := util.GetSession(c)
	if authData.AuthM.(bool) {
		mid := authData.Mid
		res := util.ReqGet(env.API_HARE_URL + "merchant/show/" + mid.(string), env.API_HARE_TOKEN)
		c.HTML(200, "merchant_settings", gin.H{
			"Data": res["data"],
			"G": global.G,
			"Page": "settings",
		})
	} else {
		c.Redirect(302, "/login")
	}
}

func SettingsProfile(c *gin.Context) {
	authData := util.GetSession(c)
	if authData.AuthM.(bool) {
		mid := authData.Mid
		res := util.ReqGet(env.API_HARE_URL + "merchant/show/" + mid.(string), env.API_HARE_TOKEN)
		c.HTML(200, "merchant_settings_profile", gin.H{
			"Data": res["data"],
			"G": global.G,
		})
	} else {
		c.Redirect(302, "/login")
	}
}

func SettingsProfileSave(c *gin.Context) {
	authData := util.GetSession(c)
	if authData.AuthM.(bool) {
		mid := authData.Mid

		rb, _ := json.Marshal(DataMap{
			"merchant_id": mid,
			"merchant_name": c.PostForm("merchant_name"),
			"merchant_logo": c.PostForm("merchant_logo"),
			"merchant_address": "-",
			"merchant_phone": "-",
		})

		res := util.ReqPut(env.API_HARE_URL + "merchant/update", env.API_HARE_TOKEN, rb)
		c.JSON(200, gin.H{
			"Status": res["status"],
		})
	} else {
		c.Redirect(302, "/login")
	}
}

func SettingsBanners(c *gin.Context) {
	authData := util.GetSession(c)
	if authData.AuthM.(bool) {
		mid := authData.Mid.(string)
		res := util.ReqGet(env.API_HARE_URL + "merchant/banner/" + mid, env.API_HARE_TOKEN)
		// log.Println("URL: " + env.API_HARE_URL)
		c.HTML(200, "merchant_banners", gin.H{
			"auth": authData,
			"Data": res["data"],
			"G": global.G,
		})
	} else {
		c.Redirect(302, "/login")
	}
}

func SettingsBannerDelete(c *gin.Context) {
	img := c.PostForm("img")
	rb, _ := json.Marshal(DataMap{"merchant_banner_id":img})
	res := util.ReqPost(env.API_HARE_URL + "merchant/banner/delete", env.API_HARE_TOKEN, rb)
	c.JSON(200, gin.H {
		"Status": res["status"],
	})
}

func Statistic(c *gin.Context) {
	header := DataMap{"title":"Statistic", "url":"/merchant"}
	c.HTML(200, "merchant_statistic", gin.H{
		"Header": header,
		"G": global.G,
	})
}