package cmerchant

import (
	"log"
	"encoding/json"
	"strconv"
    "utils"
	"constants"
	// "envi"
	"global"
    
    "github.com/gin-gonic/gin"
)

// var env = envi.Config

func Catalogue(c *gin.Context) {
    authData := util.GetSession(c)

	if authData.AuthM.(bool) {
		log.Println("MID" + authData.Mid.(string))
		res := util.ReqGet(env.API_HARE_URL + "menubook/merchant/" + authData.Mid.(string), env.API_HARE_TOKEN)
		header := DataMap{"title":"Catalogue", "url":"/merchant"}
		c.HTML(200, "merchant_catalogue", gin.H{
			"Header": header,
			"Data": res["data"],
		})
	} else {
		c.Redirect(302, "/login")
	}
}

func CataloguePage(c *gin.Context) {
	authData := util.GetSession(c)
	if authData.AuthM.(bool) {
		pageid := c.Param("pageid")
		pageidDecrypted := util.DecryptGeneral(pageid)

		// Fetch page data
		res := util.ReqGet(env.API_HARE_URL + "menubook/merchant/content/" + pageidDecrypted, env.API_HARE_TOKEN)

		header := DataMap{"title":"Page", "url":"/merchant/catalogue"}
		layout2_row := []string{"1","2"}
		layout3_row := []string{"1","2","3","4","5","6","7","8","9"}

		c.HTML(200, "merchant_catalogue_page", gin.H{
			"Header": header,
			"G": global.G,
			"Data": res["data"],
			"Title": res["merchant_menubook_title"],
			"Page": pageid,
			"Layout2": layout2_row,
			"Layout3": layout3_row,
		})
	} else { c.Redirect(302, "/login") }
}

func CataloguePageAdd(c *gin.Context) {
	authData := util.GetSession(c)
	if authData.AuthM.(bool) {
		// pageid := c.Param("pageid")

		header := DataMap{"title":"Tambah", "url":"/merchant/catalogue"}
		c.HTML(200, "merchant_catalogue_page_add", gin.H{
			"Header": header,
		})
	} else { c.Redirect(302, "/login") }
}

func PageCreate(c *gin.Context) {
	authData := util.GetSession(c)
	mid := authData.Mid
	title := c.PostForm("title")

	rb, _ := json.Marshal(DM{
		"merchant_id": mid,
		"merchant_menubook_page": "2",
		"merchant_menubook_title": title,
	})
	res := util.ReqPost(env.API_HARE_URL + "menubook/store", env.API_HARE_TOKEN, rb)
	c.JSON(200, gin.H{
		"Status": res["status"],
	})
}

func CataloguePageAddSubmit(c *gin.Context) {
	authData := util.GetSession(c)
	if authData.AuthM.(bool) {
		// pageid := c.Param("pageid")

		c.HTML(200, "merchant_catalogue_page", nil)
	} else { c.Redirect(302, "/login") }
}

func CatalogueLayoutAdd(c *gin.Context) {
	layout := c.PostForm("layout")
	pageid := util.Decrypt(c.PostForm("pageid"), constants.AesKeyTable)
	section := ""
	
	switch layout {
		case "1":
			section = "1"
		case "2":
			section = "1,2"
		case "3":
			section = "1"
		case "4", "5":
			section = "1,2,3"
	}

	// Insert content
	rbc, _ := json.Marshal(DM{
		"merchant_menubook_id": pageid,
		"menubook_content_index": 2,
		"menubook_content_layout": layout,
		"menubook_content_title": "",
	})
	resc := util.ReqPost(env.API_HARE_URL + "menubook/content/store", env.API_HARE_TOKEN, rbc)
	if resc["status"] == 200.0 {
		rescData := resc["data"].(map[string]interface{})
		content_id := rescData["menubook_content_id"]

		// Insert col
		rb, _ := json.Marshal(DM{
			"menubook_content_id": content_id,
			"menubook_col_section": section,
		})
		res := util.ReqPost(env.API_HARE_URL + "menubook/col/store", env.API_HARE_TOKEN, rb)
		
		c.JSON(200, gin.H{
			"Status": res["status"],
			"Col": res["data"],
		})
	} else {
		c.JSON(200, gin.H{
			"Status": 409,
		})
	}
}

func CatalogueSearchMenu(c *gin.Context) {
	authData := util.GetSession(c)
	mid := authData.Mid
	search := c.PostForm("search")

	// Get data menu
	rb, err := json.Marshal(DataMap{
		"merchant_id": mid,
		"limit": 25,
		"page": 1,
		"query": search,
		"menu_section_id": "",
		"order_by": "menu_name",
		"sort": "asc",
	})
	if err != nil { log.Println(err) }

	res := util.ReqPost(env.API_HARE_URL + "menu/merchant", env.API_HARE_TOKEN, rb)

	c.HTML(200, "merchant_menu_data", gin.H{
		"data": res["data"],
		"G": global.G,
		"Type": "catalogue",
	})
}

func ContentAppend(c *gin.Context) {
	col_id := c.PostForm("col_id")
	log.Println(col_id)
	var col_id_map []interface{}
	json.Unmarshal([]byte(col_id), &col_id_map)

	layout := c.PostForm("layout")
	layoutFloat, _ := strconv.ParseFloat(layout, 64)
	col := []DM{}
	
	switch layout {
	case "1":
		col = []DM{
			DM{
				"menubook_col_id": col_id_map[0],
				"data": []DM{},
			},
		}
	case "2":
		col = []DM{
			DM{
				"menubook_col_id": col_id_map[0],
				"data": []DM{},
			},
			DM{
				"menubook_col_id": col_id_map[1],
				"data": []DM{},
			},
		}
	}
	
	c.HTML(200, "merchant_catalogue_content", gin.H{
		"menubook_content_layout": layoutFloat,
		"col": col,
	})
}

func ContentDataAdd(c *gin.Context) {
	menu_id := c.PostForm("menu_id")
	data_id := c.PostForm("data_id")
	col_id := c.PostForm("col_id")

	log.Println("DATAID" + data_id)
	if data_id == "" {
		log.Println("INSERT")
		// Insert
		rb, _ := json.Marshal(DM{
			"menubook_col_id": col_id,
			"menubook_data_text": "",
			"menu_id": menu_id,
		})
		res := util.ReqPost(env.API_HARE_URL + "menubook/data/store", env.API_HARE_TOKEN, rb)
		resData := res["data"].(map[string]interface{})
		c.JSON(200, gin.H{
			"Status": res["status"],
			"Menuid": resData["menubook_data_id"],
		})
	} else {
		log.Println("UPDATE")
		// Update
		rb, _ := json.Marshal(DM{
			"menubook_data_id": data_id,
			"menubook_col_id": col_id,
			"menubook_data_text": "",
			"menu_id": menu_id,
		})
		res := util.ReqPut(env.API_HARE_URL + "menubook/data/update", env.API_HARE_TOKEN, rb)
		c.JSON(200, gin.H{
			"Status": res["status"],
		})
	}
}