package cmerchant

import (
	// "log"
	// "encoding/json"
    "utils"
	// "constants"
	// "envi"
	// "global"
    
    "github.com/gin-gonic/gin"
)

func MenuDelete(c *gin.Context) {
	menu_id := util.DecryptGeneral(c.PostForm("menu_id"))
	res := util.ReqPost(env.API_HARE_URL + "menu/destroy/" + menu_id, env.API_HARE_TOKEN, nil)
	c.JSON(200, gin.H{
		"Status": res["status"],
	})
}