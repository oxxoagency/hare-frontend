package auth

import (
	"github.com/gin-gonic/gin"
	"log"
)

func Routez(route *gin.Engine) {
    menu := route.Group("/menu")
    menu.GET("/:id", func(c *gin.Context) {
        id := c.Param("id")
        log.Println(id)
    })
}