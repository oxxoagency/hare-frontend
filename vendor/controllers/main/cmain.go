package cmain

import(
	"encoding/json"

	"hare/envi"
	"global"
	"utils"

	"github.com/gin-gonic/gin"
)

var env = envi.Config
type DM map[string]interface{}

func Home(c *gin.Context) {
	c.HTML(200, "home", gin.H{
		"G": global.G,
	})
}

func QrScanner(c *gin.Context) {
	c.HTML(200, "qr", gin.H{
		"S3": env.URL_S3,
		"Cdn": env.URL_CDN,
		"Asset": env.URL_ASSET,
		"G": global.G,
	})
}

func QrScanner2(c *gin.Context) {
	c.HTML(200, "qr_scanner", gin.H{
		"S3": env.URL_S3,
		"Cdn": env.URL_CDN,
		"Asset": env.URL_ASSET,
		"G": global.G,
	})
}

func ContactUs(c *gin.Context) {
	rb, _ := json.Marshal(DM{
		"client": 7,
		"data": DM{
			"email": c.PostForm("email"),
			"phone": c.PostForm("phone"),
			"name": c.PostForm("name"),
			"content": c.PostForm("content"),
		},
		"notif": "",
	})
	res := util.ReqPost(env.API_OXXO_URL + "contact", env.API_OXXO_TOKEN, rb)
	c.JSON(200, gin.H{
		"status": res["status"],
	})
}