package auth

import (
    "log"
    
    "github.com/gin-gonic/gin"
)

func Routes(route *gin.Engine) {
	auth := route.Group("/auth")
	auth.GET("/login", func(c *gin.Context) {
		log.Println("login")
	})
}