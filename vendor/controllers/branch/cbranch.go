package cbranch

import (
	"log"
	"time"
	// "strings"
	// "net/http"
	// "html/template"
	"encoding/json"

	"utils"
	"constants"
	"global"
	"hare/envi"
	
	"github.com/gin-gonic/gin"
)

var env = envi.Config;
var tmpl = constants.TmplRoot
var tmpl_site = constants.TmplSite
var tmpl_common = constants.TmplCommon
type DataMap map[string]interface{}
type DM map[string]interface{}

func Dashboard(c *gin.Context) {
	authData := util.GetSession(c)

	if authData.AuthB.(bool) {
		bid := authData.Bid.(string)

		res := util.ReqGet(env.API_HARE_URL + "branch/detail/" + bid, env.API_HARE_TOKEN)

		// Dashboard menu
		menu := []DM{
			DM{"t":"Orders", "u":"/branch/new", "i":"order2", "n":"order"},
			DM{"t":"Service", "u":"/branch/service", "i":"service", "n":"bell"},
			DM{"t":"Tables", "u":"/branch/table", "i":"table2", "n":""},
			DM{"t":"Transaction", "u":"/branch/transaction", "i":"order", "n":""},
		}

		c.HTML(200, "branch_dashboard", gin.H{
			"auth": authData,
			"bid": bid,
			"data": res["data"],
			"G": global.G,
			"Menu": menu,
		})
	} else {
		c.Redirect(302, "/login")
	}
}

func OrderNew(c *gin.Context) {
	authData := util.GetSession(c)

	if authData.AuthB.(bool) {
		bid := authData.Bid.(string)
		resData := FetchBranchOrder(bid, "0", "table")
		header := DataMap{"title":"View Orders", "url":"/branch"}

		c.HTML(200, "branch_order_new", gin.H{
			"auth": authData,
			"bid": bid,
			"data": resData,
			"G": global.G,
			"Status": "0",
			"Header": header,
			"Category": "table",
		})
	} else {
		c.Redirect(302, "/login")
	}
}

func LoadBranchOrder(c *gin.Context) {
	authData := util.GetSession(c)

	if authData.AuthB.(bool) {
		bid := authData.Bid.(string)
		status := c.PostForm("status")
		category := c.PostForm("category")
		resData := FetchBranchOrder(bid, status, category)

		var tmpl string
		if category == "table" { 
			tmpl = "branch_order_load" 
		} else { 
			tmpl = "branch_order_menu_load" 
		}

		c.HTML(200, tmpl, gin.H{
			"bid": bid,
			"data": resData,
			"S3": env.URL_S3,
			"Status": status,
			"G": global.G,
			"Category": category,
		})
	}
}

func FetchBranchOrder(bid string, status string, category string) (resData interface{}) {
	dt := time.Now()
	order_date := dt.Format("2006-01-02")
	log.Println("DATE" + order_date)
	
	rb, err := json.Marshal(DataMap{
		"branch_id": bid,
		"limit": 10,
		"page": 1,
		"status": status,
		"order_by": "asc",
		"order_date": order_date,
	})
	if err != nil { log.Println(err) }

	var res interface{};
	if category == "table" {
		res = util.ReqPost(env.API_HARE_URL + "order/new", env.API_HARE_TOKEN, rb)
	} else {
		res = util.ReqPost(env.API_HARE_URL + "order/new/menu", env.API_HARE_TOKEN, rb)
	}

	return res
}

func PayOrder(c *gin.Context) {
	order_id := c.PostForm("order_id")

	rb, err := json.Marshal(DataMap{
		"order_id": order_id,
		"status": "2",
	})
	if err != nil { log.Println(err) }

	res := util.SendReqPut(env.API_HARE_URL + "order/update", env.API_HARE_TOKEN, rb)
	var resMap DataMap
	json.Unmarshal([]byte(res), &resMap)

	resStatus := resMap["status"]
	c.JSON(200, gin.H{
		"status": resStatus,
	})
}

func UpdateOrderMenu(c *gin.Context) {
	omid := c.PostForm("om_id")
	rb, err := json.Marshal(DataMap{
		"order_menu_id": omid,
		"status": "2",
	})
	if err != nil { log.Println(err) }

	res := util.SendReqPut(env.API_HARE_URL + "ordermenu/update", env.API_HARE_TOKEN, rb)
	var resMap DataMap
	json.Unmarshal([]byte(res), &resMap)

	resStatus := resMap["status"]
	c.JSON(200, gin.H{
		"status": resStatus,
	})
}

func BranchEdit(c *gin.Context) {
	authData := util.GetSession(c)
	bid := authData.Bid.(string)

	res := util.ReqGet(env.API_HARE_URL + "branch/detail/" + bid, env.API_HARE_TOKEN)

	c.HTML(200, "branch_edit", gin.H{
		"data": res["data"],
		"G": global.G,
	})
}

func BranchTable(c *gin.Context) {
	authData := util.GetSession(c)

	if authData.AuthB.(bool) {
		bid := authData.Bid.(string)

		rb, _ := json.Marshal(DataMap{
			"branch_id": bid,
			"limit": 20,
			"page": 1,
			"query": "",
		})
		res := util.ReqPost(env.API_HARE_URL + "branch/table", env.API_HARE_TOKEN, rb)
		header := DataMap{"title":"Tables", "url":"/branch"}

		c.HTML(200, "branch_table", gin.H{
			"Res": res,
			"auth": authData,
			"G": global.G,
			"data": res["data"],
			"Header": header,
		})
	} else {
		c.Redirect(302, "/login?from=branch/table")
	}
}

func BranchTableNew(c *gin.Context) {
	authData := util.GetSession(c)

	if authData.AuthB.(bool) {
		header := DataMap{"title":"New Table", "url":"/branch/table"}
		c.HTML(200, "branch_table_new", gin.H{
			"auth": authData,
			"G": global.G,
			"Header": header,
		})
	} else {
		c.Redirect(302, "/login?from=branch/table")
	}
}

func BranchTableAdd(c *gin.Context) {
	authData := util.GetSession(c)
	bid := authData.Bid.(string)

	rb, _ := json.Marshal(DataMap{
		"branch_id": bid,
		"table_name": c.PostForm("table_name"),
	})
	res := util.ReqPost(env.API_HARE_URL + "table/store", env.API_HARE_TOKEN, rb)
	c.JSON(200, gin.H{
		"Status": res["status"],
	})
}

func BranchTableDetail(c *gin.Context) {
	authData := util.GetSession(c)
	table_id := c.Param("id")
	bid := authData.Bid.(string)
	hostname := c.Request.Host
	// urlMap := strings.Split(hostname, ".")s
	// host := urlMap[0]
	log.Println("HOST" + hostname)

	if authData.AuthB.(bool) {
		rb, _ := json.Marshal(DataMap{
			"branch_id": bid,
			"table_id": table_id,
		})
		res := util.ReqPost(env.API_HARE_URL + "table/show", env.API_HARE_TOKEN, rb)
		// resData := res["data"].([]interface{})[0].(map[string]interface{})
		encTid := util.Encrypt(table_id, constants.AesKeyTable)

		encTpid := ""
		resData := res["data"].(map[string]interface{})
		if resData["table_pass_id"] != nil {
			resTpid := util.FloatToString(resData["table_pass_id"].(float64))
			encTpid = util.Encrypt(resTpid, constants.AesKeyTable)
		}

		header := DataMap{"title":"Table Detail", "url":"/branch/table"}
		if res["status"] == 200.0 {
			c.HTML(200, "branch_table_detail", gin.H{
				"Res": res,
				"auth": authData,
				"Tid": encTid,
				"Tpid": encTpid,
				"Host": hostname,
				"Header": header,
			})
		} else {
			c.Redirect(302, "/")	
		}
	} else {
		c.Redirect(302, "/login?from=branch/table_detail/" + table_id)
	}
}

func TableSearch(c *gin.Context) {
	authData := util.GetSession(c)
	bid := authData.Bid.(string)

	rb, _ := json.Marshal(DataMap{
		"branch_id": bid,
		"limit": 20,
		"page": 1,
		"query": c.PostForm("search"),
	})
	res := util.ReqPost(env.API_HARE_URL + "branch/table", env.API_HARE_TOKEN, rb)

	c.HTML(200, "branch_table_data", gin.H{
		"data": res["data"],
		"G": global.G,
	})	
}

func TablePass(c *gin.Context) {
	tid := util.Decrypt(c.PostForm("tid"), constants.AesKeyTable)
	pass := c.PostForm("pass")

	rb, _ := json.Marshal(DataMap{
		"table_id": tid,
		"table_password": pass,
	})
	log.Println(tid + "|"+pass)
	res := util.ReqPost(env.API_HARE_URL + "tablepass/check", env.API_HARE_TOKEN, rb)

	encpass := util.Encrypt(pass, constants.AesKeyTablePass)
	util.Decrypt(encpass, constants.AesKeyTablePass)

	if res["status"] == 200.0 {
		encpass := util.Encrypt(pass, constants.AesKeyTablePass)
		c.JSON(200, gin.H{
			"status": 200,
			"p": encpass,
		})	
	} else {
		// encpass := util.Encrypt(pass)
		// util.Decrypt(encpass)
		c.JSON(200, gin.H{
			"status": res["status"],
			"data": res["data"],
		})
	}
}

func Service(c *gin.Context) {
	// authData := util.GetSession(c)
	// bid := authData.Bid.(string)

	res := FetchTableService(c, "0")
	header := DataMap{"title":"Service", "url":"/branch"}

	c.HTML(200, "branch_service", gin.H{
		"Data": res["data"],
		"Header": header,
		"G": global.G,
		"CurrentStatus": "0",
	})
}

func TableServiceDone(c *gin.Context) {
	id := c.PostForm("id")
	rb, _ := json.Marshal(DataMap{"table_bell_id": id})
	res := util.ReqPost(env.API_HARE_URL + "tablebell/delete", env.API_HARE_TOKEN, rb)
	c.JSON(200, gin.H{
		"Status": res["status"],
	})
}

func TableServiceLoad(c *gin.Context) {
	status := c.PostForm("status")
	res := FetchTableService(c, status)
	c.HTML(200, "branch_service_data", gin.H{
		"Data": res["data"],
		"G": global.G,
		"CurrentStatus": status,
	})
}

func FetchTableService(c *gin.Context, status string) (util.DataMap) {
	authData := util.GetSession(c)
	bid := authData.Bid.(string)

	rb, _ := json.Marshal(DataMap{
		"branch_id": bid,
		"status": status,
		"limit": 20,
		"page": 1,
	})
	res := util.ReqPost(env.API_HARE_URL + "tablebell/new", env.API_HARE_TOKEN, rb)
	return res
}

func Menu(c *gin.Context) {
	authData := util.GetSession(c)
	bid := authData.Bid.(string)

	header := DataMap{"title":"Menu", "url":"/branch"}

	res := FetchBranchMenu(c, "", "")
	rbS, _ := json.Marshal(DataMap{"branch_id": bid})
	resSection := util.ReqPost(env.API_HARE_URL + "section/branch", env.API_HARE_TOKEN, rbS)

	c.HTML(200, "branch_menu", gin.H{
		"Header": header,
		"data": res["data"],
		"G": global.G,
		"Type": "branch",
		"Section": resSection["data"],
	})
}

func MenuLoad(c *gin.Context) {
	query := ""
	sid := c.PostForm("sid")
	res := FetchBranchMenu(c, query, sid)
	c.HTML(200, "merchant_menu_data", gin.H{
		"data": res["data"],
		"G": global.G,
		"Type": "branch",
	})
}

func FetchBranchMenu(c *gin.Context, query string, sid string) (util.DataMap) {
	authData := util.GetSession(c)
	bid := authData.Bid.(string)

	rb, _ := json.Marshal(DataMap{
		"branch_id": bid, 
		"limit": 25,
		"page": 1,
		"query": query,
		"menu_section_id": sid,
		"order_by": "asc",
	})
	res := util.ReqPost(env.API_HARE_URL + "menu/branch", env.API_HARE_TOKEN, rb)
	return res
}

func MenuUnavailable(c *gin.Context) {
	authData := util.GetSession(c)
	bid := authData.Bid.(string)
	
	unavailable_id := c.PostForm("unavailable_id")

	if(unavailable_id != "") {
		log.Println("YES")
		rb, _ := json.Marshal(DataMap{
			"menu_unavailable_id": unavailable_id,
		})
		res := util.ReqPost(env.API_HARE_URL + "menu/unavailable/delete", env.API_HARE_TOKEN, rb)
		c.JSON(200, gin.H{
			"Status": res["status"],
		})
	} else {
		log.Println("NO")
		// Insert unavailable
		menu_id := c.PostForm("menu_id")
		rb, _ := json.Marshal(DataMap{
			"branch_id": bid,
			"menu_id": menu_id,
		})
		res := util.ReqPost(env.API_HARE_URL + "menu/unavailable/store", env.API_HARE_TOKEN, rb)
		c.JSON(200, gin.H{
			"Status": res["status"],
			"Data": res["data"],
		})
	}
}

func Transaction(c *gin.Context) {
	header := DataMap{"title":"Transactions", "url":"/branch"}
	c.HTML(200, "branch_transaction", gin.H{
		"Header": header,
	})
}