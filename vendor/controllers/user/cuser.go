package cuser

import (
	"log"
	"net/http"
	// "html/template"
	"encoding/json"
	"strconv"
	"reflect"

	"utils"
	"constants"
	"global"
	"hare/envi"
	
	"github.com/gorilla/sessions"
	"github.com/gin-gonic/gin"
)

var env = envi.Config
var tmpl = constants.TmplRoot
var tmpl_site = constants.TmplSite
var tmpl_common = constants.TmplCommon
type DataMap map[string]interface{}

var store = sessions.NewCookieStore([]byte(constants.SessionStore))

func Login(c *gin.Context) {
	branch := c.Param("branch")
	log.Println(branch)

	c.HTML(200, "login", gin.H{
		"title": "test",
		"G": global.G,
		"Branch": branch,
	})
}

func LoginAuth(c *gin.Context) {
	user := c.PostForm("user")
	pass := c.PostForm("pass")
	branch := c.PostForm("branch")
	// acc := c.PostForm("acc")

	var resStatus int

	if branch == "branch" {
		mid := c.PostForm("mid")

		rb, err := json.Marshal(DataMap{
			"merchant": mid,
			"user": user,
			"pass": pass,
		})
		if err != nil { log.Fatalln(err) }

		res := util.ReqPost(env.API_HARE_URL + "branch/login", env.API_HARE_TOKEN, rb)
		resStatus = int(res["status"].(float64))

		if resStatus == 200 {
			resData := res["data"].(map[string]interface{})
			branchIdF := resData["branch_id"].(float64)
			branchId := strconv.FormatFloat(branchIdF, 'f', 0, 64)

			util.SetSession(c, false, "", false, "", true, branchId, "")
		}
	} else {
		rb, err := json.Marshal(DataMap{
			"client": 7,
			"data": DataMap{
				"user": user,
				"pass": pass,
			},
		})
		if err != nil { log.Println(err) }
		res := util.ReqPost(env.API_OXXO_URL + "login_client_user", env.API_OXXO_TOKEN, rb)
		resStatus = int(res["status"].(float64))

		if resStatus == 200 {
			resData := res["data"].(map[string]interface{})
			userIdF := resData["user_id"].(float64)
			userId := strconv.FormatFloat(userIdF, 'f', 0, 64)

			// Get merchant info
			firstMid := 0.0
			resM := util.ReqGet(env.API_HARE_URL + "dashboard/user/" + userId, env.API_HARE_TOKEN)
			if resM["status"] == 200.0 {
				resMerchant := resM["merchant"].([]interface{})
				merchantData := resMerchant[0].(map[string]interface{})
				// log.Println("ASDSA" + merchantData["merchant_id"].(string))
				firstMid = merchantData["merchant_id"].(float64)
			}

			util.SetSession(c, true, userId, true, util.FloatToString(firstMid), false, "", "")
		}
	}

	c.JSON(http.StatusOK, gin.H{
		"status": resStatus,
	})

	/*if acc == "m" {
		rb, err := json.Marshal(queryMap)
		if err != nil { log.Fatalln(err) }

		res := util.ReqPost(env.API_OXXO_URL + "login_client_user", env.API_OXXO_TOKEN, rb)
		resStatus = int(res["status"].(float64))

		if resStatus == 200 {
			resData := res["data"].(map[string]interface{})
			userIdF := resData["user_id"].(float64)
			userId := strconv.FormatFloat(userIdF, 'f', 0, 64)

			if acc == "m" {
				// c auth authM uid bid mid uname
				util.SetSession(c, false, true, "", "", userId, "")
			} else if acc == "b" {
				util.SetSession(c, true, false, "", userId, "", "")
			}
		}
	} else {
		mid := c.PostForm("mid")
		queryMap["merchant"] = mid

		rb, err := json.Marshal(queryMap)
		if err != nil { log.Fatalln(err) }

		res := util.ReqPost(env.API_OXXO_URL + "login_client_user", env.API_OXXO_TOKEN, rb)
		resStatus = int(res["status"].(float64))

		if resStatus == 200 {
			resData := res["data"].(map[string]interface{})
			branchIdF := resData["branch_id"].(float64)
			branchId := strconv.FormatFloat(branchIdF, 'f', 0, 64)

			util.SetSession(c, true, false, "", branchId, "", "")
		}
	}

	c.JSON(http.StatusOK, gin.H{
		"status": resStatus,
	})*/
}

func Logout(c *gin.Context) {
	util.SetSession(c, false, "", false, "", false, "", "")
	c.JSON(200, gin.H{
		"status": 200,
	})
}

func Register(c *gin.Context) {
	c.HTML(200, "register", gin.H{
		"G": global.G,
	})
}

func RegisterEmail(c *gin.Context) {
	email := c.PostForm("email")

	rb, err := json.Marshal(DataMap{
		"client": 7,
		"data": DataMap{
			"user_email": email,
			"verify_link": "https://hare.co.id/register/verify?verify=",
		},
	})
	if err != nil { log.Fatalln(err) }

	res := util.SendReqPost(env.API_OXXO_URL + "register_client_user", env.API_OXXO_TOKEN, rb)
	var resMap DataMap
	json.Unmarshal([]byte(res), &resMap)
	resStatus := int(resMap["status"].(float64))
	c.JSON(http.StatusOK, gin.H {
		"status": resStatus,
	})
}

func RegisterVerify(c *gin.Context) {
	verify := c.Request.URL.Query().Get("verify")
	
	rb, err := json.Marshal(DataMap{
		"client": 7,
		"data": DataMap{
			"user_verify": verify,
		},
	})
	if err != nil { log.Fatalln(err) }
	// log.Println("adasd" + verify)

	res := util.SendReqPost(env.API_OXXO_URL + "register/verify", env.API_OXXO_TOKEN, rb)
	var resMap DataMap
	json.Unmarshal([]byte(res), &resMap)
	resStatus := int((resMap["status"].(float64)))
	
	if(resStatus == 200) {
		email := resMap["data"]
		c.HTML(200, "register_verify", gin.H{
			"email": email,
			"Asset": env.URL_ASSET,
		})
	} else {
		c.HTML(200, "page_404", nil)
	}
}

func RegisterUsername(c *gin.Context) {
	email := c.PostForm("email")
	pass := c.PostForm("pass")
	username := c.PostForm("username")

	rb, err := json.Marshal(DataMap{
		"client": 7,
		"data": DataMap{
			"user_email": email,
			"user_password": pass,
			"user_username": username,
		},
	})
	if err != nil { log.Println(err) }

	res := util.SendReqPost(env.API_OXXO_URL + "register/afterverify", env.API_OXXO_TOKEN, rb)
	var resMap DataMap
	json.Unmarshal([]byte(res), &resMap)

	resStatus := int(resMap["status"].(float64))

	if resStatus == 200 {
		// Set session
		resDataMap := resMap["data"].(map[string]interface{})
		uidF := resDataMap["user_id"].(float64)
		uid := strconv.FormatFloat(uidF, 'f', 0, 64)
		uname := resDataMap["user_username"].(string)

		// c auth authm uid bid mid uname
		util.SetSession(c, true, uid, false, "", false, "", uname)
	}

	c.JSON(200, gin.H{
		"status": resStatus,
	})
}

func Dashboard(c *gin.Context) {
	var authData = util.GetSession(c)
	auth := authData.Auth.(bool)
	log.Println(auth)

	if auth {
		uid := authData.Uid.(string)
		log.Println("UID" + uid)
		
		res := util.ReqGet(env.API_HARE_URL + "dashboard/user/" + uid, env.API_HARE_TOKEN)

		var merchantStatus string
		if res["merchant"] != nil {
			rt := reflect.TypeOf(res["merchant"])
			log.Println(rt)

			if rt.String() == "string" {
				merchantStatus = "404"
			} else {
				merchantStatus = "200"
				merchantArr := res["merchant"].([]interface{})
				merchantData := merchantArr[0].(map[string]interface{})
				midF := merchantData["merchant_id"].(float64)
				mid := strconv.FormatFloat(midF, 'f', 0, 64)
				log.Println("MID" + mid)

				util.SetSessionMerchant(c, true, mid)
			}
		}
		
		c.HTML(200, "user_dashboard", gin.H{
			"auth": authData,
			"user": res["user"],
			"merchant": res["merchant"],
			"G": global.G,
			"merchantStatus": merchantStatus,
		})
	} else {
		c.Redirect(302, "/login")
	}
}