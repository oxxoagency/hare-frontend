package ctest

import(
	"utils"
	"log"
	"encoding/json"
	"hare/envi"

	"github.com/gin-gonic/gin"
)

var env = envi.Config
type DM map[string]interface{}

func Textarea(c *gin.Context) {
	rb, _ := json.Marshal(DM{"user_id":20})
	res := util.ReqPost(env.API_HARE_URL + "user/profile", env.API_HARE_TOKEN, rb)
	log.Println(res)

	c.HTML(200, "test_textarea", gin.H{
		"Data": res["data"],
	})
}