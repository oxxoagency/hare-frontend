package ctest

import (
	"fmt"
	"log"
	// "time"

	"github.com/xendit/xendit-go"
	"github.com/xendit/xendit-go/invoice"
	"github.com/gin-gonic/gin"
)

func ExampleCreate(c *gin.Context) {
	xendit.Opt.SecretKey = "xnd_production_8wLQ9wFpwm5778Q2xJNGH3uQeHiygSfNdcTAft1ur4HPooO4oSq6slrVVblA9N6"

	data := invoice.CreateParams{
		ExternalID:  "invoice-example",
		Amount:      200000,
		PayerEmail:  "customer@customer.com",
		Description: "invoice #1",
	}

	resp, err := invoice.Create(&data)
	if err != nil {
		log.Fatal(err)
	}

	fmt.Printf("created invoice: %+v\n", resp)
}