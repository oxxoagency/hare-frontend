package ctable

import (
	"log"
	"strconv"
	"time"
	"encoding/json"

	"utils"
	"constants"
	// "hare/global"
    "hare/envi"
	
	"github.com/gin-gonic/gin"
)

var env = envi.Config;
type DM map[string]interface{}

func TableOpen(c *gin.Context) {
	tid := util.Decrypt(c.PostForm("tid"), constants.AesKeyTable)
	
	// Generate password
	ttime := strconv.FormatInt(time.Now().Unix(), 10)
	tpass := ttime[len(ttime)-4:]
	log.Println(ttime + " | " + tpass)

	// Send password
	rb, _ := json.Marshal(DM{
		"table_id": tid,
		"table_password": tpass,
	})
	res := util.ReqPost(env.API_HARE_URL + "tablepass/open", env.API_HARE_TOKEN, rb)

	c.JSON(200, gin.H{
		"status": res["status"],
	})
}

func TableOrderCheck(c *gin.Context) {
	tid := util.Decrypt(c.PostForm("tid"), constants.AesKeyTable)
	rbO, _ := json.Marshal(DM{ "table_id": tid, })
	resO := util.ReqPost(env.API_HARE_URL + "order/menu/table", env.API_HARE_TOKEN, rbO)
	c.JSON(200, gin.H{
		"Order": len(resO["data"].([]interface{})),
	})
}

func TableClose(c *gin.Context) {
	// Check if there is order, delete order
	statusTable := 0.0
	order := c.PostForm("order")
	if order == "1" {
		tid := util.Decrypt(c.PostForm("tid"), constants.AesKeyTable)
		rbt, _ := json.Marshal(DM{"table_id": tid})
		resT := util.ReqPost(env.API_HARE_URL + "order/active/delete", env.API_HARE_TOKEN, rbt)
		statusTable = resT["status"].(float64)
	}

	tpid := util.Decrypt(c.PostForm("tpid"), constants.AesKeyTable)
	rb, _ := json.Marshal(DM{ "table_pass_id": tpid })
	res := util.ReqPost(env.API_HARE_URL + "tablepass/close", env.API_HARE_TOKEN, rb)
	c.JSON(200, gin.H{
		"status": res["status"],
		"StatusTable": statusTable,
	})
}

func TableOrder(c *gin.Context) {
	tid := util.Decrypt(c.Param("id"), constants.AesKeyTable)
	header := DM{"title":"Table Orders", "url":"/branch/table_detail/" + tid}

	rb, _ := json.Marshal(DM{ "table_id": tid, })
	res := util.ReqPost(env.API_HARE_URL + "order/menu/table", env.API_HARE_TOKEN, rb)

	total := 0.0
	if res["status"] == 200.0 {
		resData := res["data"].([]interface{})
		if len(resData) > 0 {
			resData := res["data"].([]interface{})
			resDataFirst := resData[0].(map[string]interface{})
			resMenu := resDataFirst["menus"].([]interface{})

			for _, menu := range resMenu {
				menuData := menu.(map[string]interface{})
				total += menuData["qty"].(float64) * menuData["order_menu_price"].(float64)
			}
		}
	}

	c.HTML(200, "branch_table_order", gin.H {
		"Data": res["data"],
		"Tid": c.Param("id"),
		"Tpid": c.Param("tpid"),
		"Header": header,
		"Total": total,
	})
}

func PaymentFinish(c *gin.Context) {
	oid := c.PostForm("oid")
	total := c.PostForm("total")

	rb, _ := json.Marshal(DM{
		"order_id": oid,
		"status": 2,
		"order_payment_type": 3,
		"order_payment_amount": total,
		"order_payment_id": "-",
		"order_payment_ref": "-",
	})
	res := util.ReqPut(env.API_HARE_URL + "order/update", env.API_HARE_TOKEN, rb)
	resTPStatus := 401.0
	if res["status"] == 200.0 {
		// Close table
		tid := util.Decrypt(c.PostForm("tid"), constants.AesKeyTable)
		rbt, _ := json.Marshal(DM{ "table_id": tid })
		resTP := util.ReqPost(env.API_HARE_URL + "tablepass/close/table", env.API_HARE_TOKEN, rbt)
		resTPStatus = resTP["status"].(float64)
	}

	c.JSON(200, gin.H{
		"Status": res["status"],
		"StatusTP": resTPStatus,
	})
}