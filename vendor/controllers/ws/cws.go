package cws

import (
	"fmt"
    "log"
    "encoding/json"
    "github.com/gin-gonic/gin"
    "github.com/gorilla/websocket"

    "hare/envi"
    "utils"
)

type DM map[string]interface{}

var (
    upgrader = websocket.Upgrader{}
    env = envi.Config
)

func BranchNotif(c *gin.Context) {
    ws, err := upgrader.Upgrade(c.Writer, c.Request, nil)
    if err != nil {
        // return err
    }
	defer ws.Close()

    for {
        // Read
        _, msg, err := ws.ReadMessage()
        if err != nil {
			// c.Logger().Error(err)
			// log.Println(err)
        }
        fmt.Printf("%s", msg)

        // Get notif
        authData := util.GetSession(c)
        bid := authData.Bid.(string)
        
        rb, _ := json.Marshal(DM{"branch_id":bid})
        res := util.SendReqPost(env.API_HARE_URL + "branch/notif", env.API_HARE_TOKEN, rb)
        // log.Println(res["data"])

        // Write
        // err := ws.WriteMessage(websocket.TextMessage, []byte("Hello, Client!"))
        err2 := ws.WriteMessage(websocket.TextMessage, []byte(res))
        if err != nil {
			// c.Logger().Error(err)
			log.Println(err2)
        }

        
    }
}