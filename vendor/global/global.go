package global

import (
	"hare/envi"
)

type GlobalStruct struct {
	S3			interface{}
	Cdn			interface{}
	Asset		interface{}
}

var G = GlobalStruct{
	S3: envi.Config.URL_S3,
	Cdn: envi.Config.URL_CDN,
	Asset: envi.Config.URL_ASSET,
}