package route

import (
	// "html/template"
	// "hare/controllers/auth"
	"constants"
	"controllers/main"
	"controllers/menu"
	"controllers/user"
	"controllers/merchant"
	"controllers/branch"
	"controllers/payment"
	"controllers/order"
	"controllers/table"
	"controllers/ws"
	"controllers/test"

	"utils"

	"github.com/gin-gonic/gin"
)

var tmpl = constants.TmplRoot
var tmpl_site = constants.TmplSite
var tmpl_common = constants.TmplCommon

func Routes(r *gin.Engine) {
	r.GET("/", cmain.Home)
	// auth := r.Group("/auth")
	
	// r.GET("/login2", func(c *gin.Context) {
	// 	tmpl := template.Must(template.ParseFiles(
	// 		tmpl + "base.tmpl",
	// 		tmpl_site + "login.tmpl",
	// 	))
	// 	r.SetHTMLTemplate(tmpl)
	// 	c.HTML(200, "base", nil)
	// })

	login := r.Group("/login")
	login.GET("/", cuser.Login)
	login.POST("/auth", cuser.LoginAuth)
	login.GET("/mode/:branch", cuser.Login)
	
	r.POST("/logout", cuser.Logout)

	register := r.Group("/register")
	register.GET("/", cuser.Register)
	register.POST("/email", cuser.RegisterEmail)
	register.GET("/verify", cuser.RegisterVerify)
	register.POST("/username", cuser.RegisterUsername)

	user := r.Group("/user")
	user.GET("/", cuser.Dashboard)

	menu := r.Group("/menu")
	menu.GET("/t/:id", cmenu.Show)
	menu.POST("/d/:id", cmenu.ShowDetail)
	menu.GET("/loader", cmenu.ShowDetailLoader)
	menu.POST("/help", cmenu.HelpSubmit)
	// menu.GET("/checkoutz", cmenu.Checkout)
	menu.POST("/bill/load", cmenu.BillLoad)

	order := r.Group("/order")
	order.POST("/new", cmenu.SubmitOrderNew)
	order.POST("/done", corder.Done)

	payment := r.Group("/payment")
	payment.POST("/process", cpayment.Process)
	payment.POST("/notification", cpayment.Notification)
	payment.GET("/success/:id", cpayment.Success)
	payment.GET("/failed/:id", cpayment.Failed)
	payment.GET("/pending/:id", cpayment.Pending)
	payment.POST("/xendit_invoice/finish", cpayment.XenditInvoiceFinish)
	payment.POST("/gopay/notification", cpayment.GopayNotification)
	payment.POST("/send_receipt", cpayment.SendReceipt)
	
	payment.GET("/finish", cpayment.Success)
	payment.GET("/unfinish", cpayment.Pending)
	payment.GET("/error", cpayment.Failed)
	
	merchant := r.Group("/merchant")
	merchant.GET("/", cmerchant.Dashboard)
	merchant.GET("/menu", cmerchant.Menu)
	merchant.GET("/menu_new", cmerchant.MenuAdd)
	merchant.GET("/branch", cmerchant.Branch)
	merchant.GET("/branch/new", cmerchant.BranchNew)
	merchant.GET("/new", cmerchant.New)
	merchant.GET("/menu/:menuid", cmerchant.MenuDetail)
	merchant.GET("/statistic", cmerchant.Statistic)
	merchant.GET("/settings", cmerchant.Settings)
	merchant.GET("/settings/profile", cmerchant.SettingsProfile)
	merchant.GET("/settings/banners", cmerchant.SettingsBanners)
	merchant.GET("/catalogue", cmerchant.Catalogue)
	merchant.GET("/catalogue/page/:pageid", cmerchant.CataloguePage)
	merchant.GET("/catalogue/add", cmerchant.CataloguePageAdd)

	merchant.POST("/menu_search", cmerchant.MenuSearch)
	merchant.POST("/branch/submit_new", cmerchant.BranchSubmitNew)
	merchant.POST("/submit_new", cmerchant.SubmitNew)
	merchant.POST("/menu_new_submit", cmerchant.MenuAddSubmit)
	merchant.POST("/menu_edit_submit", cmerchant.MenuEditSubmit)
	merchant.POST("/menu_delete", cmerchant.MenuDelete)
	merchant.POST("/upload_photo", cmerchant.UploadPhoto)
	merchant.POST("/section_new", cmerchant.SectionNew)
	merchant.POST("/navigate_branch", cmerchant.NavigateBranch)
	merchant.POST("/settings/profile_save", cmerchant.SettingsProfileSave)
	merchant.POST("/settings/banner_delete", cmerchant.SettingsBannerDelete)
	merchant.POST("/catalogue/page_create", cmerchant.PageCreate)
	merchant.POST("/catalogue/add_layout", cmerchant.CatalogueLayoutAdd)
	merchant.POST("/catalogue/search_menu", cmerchant.CatalogueSearchMenu)
	merchant.POST("/catalogue/add_content_data", cmerchant.ContentDataAdd)
	merchant.POST("/catalogue/content_append", cmerchant.ContentAppend)

	m := r.Group("/m/:id")
	m.GET("/", cmerchant.Dashboard)
	m.GET("/menu_new", cmerchant.MenuAdd)
	m.GET("/menu/:menuid", cmerchant.MenuDetail)
	m.GET("/menu", cmerchant.Menu)
	m.GET("/branch", cmerchant.Branch)
	
	branch := r.Group("/branch")
	branch.GET("/", cbranch.Dashboard)
	branch.GET("/new", cbranch.OrderNew)
	// // branch.GET("/order/:id", cbranch.Order)
	branch.GET("/edit", cbranch.BranchEdit)
	branch.GET("/table", cbranch.BranchTable)
	branch.GET("/table_new", cbranch.BranchTableNew)
	branch.GET("/table_detail/:id", cbranch.BranchTableDetail)
	branch.GET("/service", cbranch.Service)
	branch.GET("/menu", cbranch.Menu)
	branch.GET("/transaction", cbranch.Transaction)

	branch.POST("/pay", cbranch.PayOrder)
	branch.POST("/update_order_menu", cbranch.UpdateOrderMenu)
	branch.POST("/load_order", cbranch.LoadBranchOrder)
	branch.POST("/table_add", cbranch.BranchTableAdd)
	branch.POST("/service/done", cbranch.TableServiceDone)
	branch.POST("/service/reload", cbranch.TableServiceLoad)
	branch.POST("/menu/unavailable", cbranch.MenuUnavailable)
	branch.POST("/menu/load", cbranch.MenuLoad)

	b := r.Group("/b/:id")
	b.GET("/", cbranch.Dashboard)
	b.GET("/new", cbranch.OrderNew)
	b.POST("/load_order", cbranch.LoadBranchOrder)

	table := r.Group("/table")
	table.GET("/order/:id", ctable.TableOrder)
	table.POST("/search", cbranch.TableSearch)
	table.POST("/pass", cbranch.TablePass)
	table.POST("/open", ctable.TableOpen)
	table.POST("/close", ctable.TableClose)
	table.POST("/order/check", ctable.TableOrderCheck)
	table.POST("/order/payment_finish", ctable.PaymentFinish)

	// r.GET("/checkout", cpayment.Checkout)

	r.POST("/select_query", util.SelectQuery)

	r.GET("/qr", cmain.QrScanner)
	r.GET("/qr_scan", cmain.QrScanner2)

	r.POST("/contact_us", cmain.ContactUs)
	
	r.GET("/ws_branch", cws.BranchNotif)

	test := r.Group("/test")
	test.GET("/testovo", ctest.ExampleCreate)
	test.GET("/textarea", ctest.Textarea)
}