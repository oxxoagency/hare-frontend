package util

import (
	"net/http"
	"time"
	"bytes"
	"log"
	"errors"
	"io/ioutil"
	"encoding/json"
	"strconv"
	"crypto/aes"
	"crypto/cipher"
	"crypto/rand"
	"encoding/hex"
	"io"
	"fmt"

	"constants"
	"hare/envi"

	"github.com/gorilla/sessions"
	"github.com/gin-gonic/gin"
)

var env = envi.Config
var store = sessions.NewCookieStore([]byte(constants.SessionStore))
type DataMap map[string]interface{}

type AuthData struct {
	Auth			interface{}
	AuthM			interface{}
	AuthB			interface{}
	Uid				interface{}
	Mid				interface{}
	Bid				interface{}
	Name			interface{}
}

func SendReqPost(url string, token string, reqBody []byte) (respBody []byte) {
	timeout := time.Duration(20 * time.Second)
	client := http.Client { Timeout: timeout, }

	req, err := http.NewRequest("POST", url, bytes.NewBuffer(reqBody))
	req.Header.Set("Content-Type", "application/json")
	req.Header.Set("Authorization", token)

	if err != nil { log.Fatalln(err) }

	resp, err := client.Do(req)
	if err != nil { log.Fatalln(err) }

	defer resp.Body.Close()
	respBody, err = ioutil.ReadAll(resp.Body)
	if err != nil { log.Fatalln(err) }

	log.Println(string(respBody))
	return respBody
}

func ReqPost(url string, token string, reqBody []byte) (resMap DataMap) {
	timeout := time.Duration(30 * time.Second)
	client := http.Client { Timeout: timeout, }

	req, err := http.NewRequest("POST", url, bytes.NewBuffer(reqBody))
	req.Header.Set("Content-Type", "application/json")
	req.Header.Set("Authorization", token)

	if err != nil { log.Fatalln(err) }

	resp, err := client.Do(req)
	if err != nil { log.Fatalln(err) }

	defer resp.Body.Close()
	respBody, err := ioutil.ReadAll(resp.Body)
	if err != nil { log.Fatalln(err) }

	log.Println(string(respBody))
	json.Unmarshal([]byte(respBody), &resMap)

	return resMap
}

func SendReqPut(url string, token string, reqBody []byte) (respBody []byte) {
	timeout := time.Duration(30 * time.Second)
	client := http.Client { Timeout: timeout, }

	req, err := http.NewRequest("PUT", url, bytes.NewBuffer(reqBody))
	req.Header.Set("Content-Type", "application/json")
	req.Header.Set("Authorization", token)

	if err != nil { log.Fatalln(err) }

	resp, err := client.Do(req)
	if err != nil { log.Fatalln(err) }

	defer resp.Body.Close()
	respBody, err = ioutil.ReadAll(resp.Body)
	if err != nil { log.Fatalln(err) }

	log.Println(string(respBody))
	return respBody
}

func ReqPut(url string, token string, reqBody []byte) (resMap DataMap) {
	timeout := time.Duration(30 * time.Second)
	client := http.Client { Timeout: timeout, }

	req, err := http.NewRequest("PUT", url, bytes.NewBuffer(reqBody))
	req.Header.Set("Content-Type", "application/json")
	req.Header.Set("Authorization", token)

	if err != nil { log.Fatalln(err) }

	resp, err := client.Do(req)
	if err != nil { log.Fatalln(err) }

	defer resp.Body.Close()
	respBody, err := ioutil.ReadAll(resp.Body)
	if err != nil { log.Fatalln(err) }

	log.Println(string(respBody))
	json.Unmarshal([]byte(respBody), &resMap)

	return resMap
}

func SendReqGet(url string, token string) (respBody []byte) {
	timeout := time.Duration(30 * time.Second)
	client := http.Client { Timeout: timeout, }

	req, err := http.NewRequest("GET", url, nil)
	req.Header.Set("Content-Type", "application/json")
	req.Header.Set("Authorization", token)
	if err != nil { log.Fatalln(err) }

	resp, err := client.Do(req)
	if err != nil { log.Fatalln(err) }

	defer resp.Body.Close()
	respBody, err = ioutil.ReadAll(resp.Body)
	if err != nil { log.Fatalln(err) }

	// log.Println(string(respBody))
	return respBody
}

func ReqGet(url string, token string) (resMap DataMap) {
	timeout := time.Duration(30 * time.Second)
	client := http.Client { Timeout: timeout, }

	req, err := http.NewRequest("GET", url, nil)
	req.Header.Set("Content-Type", "application/json")
	req.Header.Set("Authorization", token)
	if err != nil { log.Fatalln(err) }

	resp, err := client.Do(req)
	if err != nil { log.Fatalln(err) }

	defer resp.Body.Close()
	respBody, err := ioutil.ReadAll(resp.Body)
	if err != nil { log.Fatalln(err) }

	log.Println(string(respBody))
	json.Unmarshal([]byte(respBody), &resMap)

	return resMap
}

func SetSession(c *gin.Context, auth bool, uid string, authm bool, mid string, authb bool, bid string, uname string) {
	session, _ := store.Get(c.Request, "hare")
	session.Values["auth"] = auth
	session.Values["authM"] = authm
	session.Values["authB"] = authb
	session.Values["uid"] = uid
	session.Values["mid"] = mid
	session.Values["bid"] = bid
	session.Values["name"] = uname
	session.Save(c.Request, c.Writer)
}

func SetSessionMerchant(c *gin.Context, authm bool, mid string) {
	session, _ := store.Get(c.Request, "hare")
	session.Values["authM"] = authm
	session.Values["mid"] = mid
	session.Save(c.Request, c.Writer)
}

func SetSessionBranch(c *gin.Context, authb bool, bid string) {
	session, _ := store.Get(c.Request, "hare")
	session.Values["authB"] = authb
	session.Values["bid"] = bid
	session.Save(c.Request, c.Writer)
}

func GetSession(c *gin.Context) (authData AuthData) {
	session, _ := store.Get(c.Request, "hare")
	authData = AuthData {
		session.Values["auth"], 
		session.Values["authM"], 
		session.Values["authB"], 
		session.Values["uid"],
		session.Values["mid"],
		session.Values["bid"],
		session.Values["name"],
	}
	return authData
}

func FloatToString(f float64) (s string) {
	s = strconv.FormatFloat(f, 'f', 0, 64)
	return s
}

func IntToString(i int) (s string) {
	s = strconv.Itoa(i)
	return s
}

func AddTS(s string) (rs string) {
	if(len(s) > 3) {
		rs = s[0:len(s)-3] + "." + s[len(s)-3:]
		if(len(rs) > 7) {
			rs = rs[0:len(rs)-7] + "." + rs[len(rs)-7:]
			if(len(rs) > 11) {
				rs = rs[0:len(rs)-11] + "." + rs[len(rs)-11:]
			}
		}
	} else {
		rs = s
	}
	return rs
}

func Mul(i1 float64, i2 float64) (float64) {
	return i1 * i2
}

func Add(i1 float64, i2 float64) (float64) {
	return i1 + i2
}

func Encrypt(str string, aeskey string) string {

	//Since the key is in string, we need to convert decode it to bytes
	key := []byte(aeskey)
	plaintext := []byte(str)

	//Create a new Cipher Block from the key
	block, err := aes.NewCipher(key)
	if err != nil { log.Println(err.Error()) }

	//Create a new GCM - https://en.wikipedia.org/wiki/Galois/Counter_Mode
	//https://golang.org/pkg/crypto/cipher/#NewGCM
	aesGCM, err := cipher.NewGCM(block)
	if err != nil { log.Println(err.Error()) }

	//Create a nonce. Nonce should be from GCM
	nonce := make([]byte, aesGCM.NonceSize())
	if _, err = io.ReadFull(rand.Reader, nonce); err != nil { log.Println(err.Error()) }

	//Encrypt the data using aesGCM.Seal
	//Since we don't want to save the nonce somewhere else in this case, we add it as a prefix to the encrypted data. The first nonce argument in Seal is the prefix.
	ciphertext := aesGCM.Seal(nonce, nonce, plaintext, nil)
	log.Println(fmt.Sprintf("%x", ciphertext))
	return fmt.Sprintf("%x", ciphertext)
}

func Decrypt(str string, aeskey string) string {

	key := []byte(aeskey)
	enc, _ := hex.DecodeString(str)

	//Create a new Cipher Block from the key
	block, err := aes.NewCipher(key)
	if err != nil { log.Println(err.Error()) }

	//Create a new GCM
	aesGCM, err := cipher.NewGCM(block)
	if err != nil { log.Println(err.Error()) }

	//Get the nonce size
	nonceSize := aesGCM.NonceSize()

	//Extract the nonce from the encrypted data
	nonce, ciphertext := enc[:nonceSize], enc[nonceSize:]

	//Decrypt the data
	plaintext, err := aesGCM.Open(nil, nonce, ciphertext, nil)
	if err != nil {
		// panic(err.Error())
		log.Println(err.Error())
		return "invalid"
	} else {
		log.Println(fmt.Sprintf("%s", plaintext))
		return fmt.Sprintf("%s", plaintext)
	}
}

func EncryptGeneral(str string) string {
	return Encrypt(str, constants.AesKeyTable)
}

func DecryptGeneral(str string) string {
	return Decrypt(str, constants.AesKeyTable)
}

func ConvertDate(str string) string {
	layoutISO := "2006-01-02 15:04:05"
	// timeStr, _ := time.Parse(time.RFC3339,str);
	timeStr, _ := time.Parse(layoutISO, str)
	timeStr.Format(time.RFC822)

	// layout := "01/02/06"
	out := "02/01/2006 15:04"

	convertTime := timeStr.Format(out)
	//convertTime, _ := time.Parse(layout, timeStr.String())
	log.Println(convertTime)
	return convertTime;
}

func CDate(strs string) string {
	// layout := "Mon 01/02/06 03:04:05PM -07:00"
	// layout := "01/02/06 03:04:05"
	// str := "Mon 03/27/17 02:21:00.215PM +02:00"

	timeStr, _ := time.Parse(time.RFC3339,strs);
	// timeStrz := timeStr.String()

	// t, err := time.Parse(layout, timeStrz)
	// if err != nil {
	// 	fmt.Println("ERROR:", err.Error())
	// }

	// out := "02/01/2006 03:04"
	// fmt.Println("DateTime:", t.Format(out))
	strreturn := timeStr.Format("2 January, 06 15:04")
	return strreturn
}

func SelectQuery(c *gin.Context) {
	table := c.PostForm("table")
	query := c.PostForm("query")
	log.Println(table)

	queryMap := DataMap{
		"query": query,
		"limit": 50,
		"page": 1,
	}
	
	// Fetch data
	var method string
	switch(table) {
		case "menu_section":
			method = "section/merchant"
			param := c.PostForm("param")
			var paramMap DataMap
			json.Unmarshal([]byte(param), &paramMap)
			log.Println("merchantid" + paramMap["merchant_id"].(string))
			
			queryMap["merchant_id"] = paramMap["merchant_id"]
		case "product_category":
			method = "productcategory/show"
	}
	log.Println("method" + method + "tab" + table)

	rb, err := json.Marshal(queryMap)
	if err != nil { log.Println(err) }

	res := ReqPost(env.API_HARE_URL + "section/merchant", env.API_HARE_TOKEN, rb)

	log.Println(res["data"])
	
	c.JSON(http.StatusOK, gin.H {
		"result": res["data"],
	})
}

func CombineVariable(values ...interface{}) (map[string]interface{}, error) {
	if len(values)%2 != 0 {
		return nil, errors.New("invalid dict call")
	}
	dict := make(map[string]interface{}, len(values)/2)
	for i := 0; i < len(values); i+=2 {
		key, ok := values[i].(string)
		if !ok {
			return nil, errors.New("dict keys must be strings")
		}
		dict[key] = values[i+1]
	}
	return dict, nil
}