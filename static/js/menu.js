$(function() {

    // Row menu click
    $('body').delegate('.row_menu_open', 'click',function() {
        let menu_id = $(this).data('menu_id');
        let qty = $(this).find('.menu_row_qty').text();
        let unavailable = $(this).data('unavailable');
        let mtype = $('#input_mtype').val();

        $('.container_menu_preview').show();
        $('.container_menu_preview').load('/menu/d/' + menu_id, 
        {
            qty: qty,
            unavailable: unavailable,
            mtype: mtype,
        }, function(r) {
            console.log(r);
        });
    });

    // Menu tab click
    $('body').delegate('.menu_tab', "click", function() {
        let section = $(this).data('section');

        $('.menu_tab_selected').addClass('menu_tab');
        $('.menu_tab_selected').removeClass('menu_tab_selected');
        $(this).removeClass('menu_tab');
        $(this).addClass('menu_tab_selected');
        
        if(section != '') {
            let menu = $('.container_menu_section[data-section="' + section + '"]');
            menu.show();
            $('.container_menu_section').not(menu).hide();
        } else {
            $('.container_menu_section').show();
        }
    });

    // Btn menu add
    $('body').delegate('.btn_menu_add', 'click', function(e) {
        e.stopPropagation();
        let parent = $(this).parent();
        $(this).hide();
        parent.find('.menu_row_qty').text('1');
        
        parent.find('.container_row_qty').show();

        let checkout_qty = parseInt(remove_dot($('#menu_btn_checkout_qty').text()));
        let checkout_amt = parseInt(remove_dot($('#menu_btn_checkout_amt').text()));

        let menu_price = parseInt(remove_dot($(this).parent().parent().parent().find('.menu_row_price_val').text()));
        checkout_qty++;
        checkout_amt += menu_price;

        $('#menu_btn_checkout_qty').text(add_dot(checkout_qty.toString()));
        $('#menu_btn_checkout_amt').text(add_dot(checkout_amt.toString()));

        $('.container_menu_checkout_cart').css({
            'bottom': '60px'
        });
    });

    // Menu detail qty plus
    $('body').delegate('.btn_menu_qty_min, .btn_menu_qty_plus', 'click', function(e) {
        e.stopPropagation();

        let checkout_qty = parseInt(remove_dot($('#menu_btn_checkout_qty').text()));
        let checkout_amt = parseInt(remove_dot($('#menu_btn_checkout_amt').text()));

        let menu_price = parseInt(remove_dot($(this).parent().parent().parent().find('.menu_row_price_val').text()));

        let btn = $(this).data('btn');
        let parent = $(this).parent();
        let qty_container = parent.find('.menu_row_qty');
        let qty = qty_container.text();
        
        if(btn == '0') {
            qty--;
            if(qty == 0) {
                parent.hide();
                parent.parent().find('.btn_menu_add').show();
            }
            checkout_qty--;
            checkout_amt -= menu_price;

            if(checkout_qty == 0) {
                $('.container_menu_checkout_cart').css({
                    'bottom': '-150px'
                });
            }
        } else {
            qty++;
            checkout_qty++;
            checkout_amt += menu_price;
        }

        qty_container.text(qty);
        $('#menu_btn_checkout_qty').text(add_dot(checkout_qty.toString()));
        $('#menu_btn_checkout_amt').text(add_dot(checkout_amt.toString()));
    });

    // Menu bot tab click
    $('.menu_bot_tab').click(function() {
        let tab = $(this).data('tab');

        if(tab != '4') {
            $(this).addClass('menu_bot_tab_selected');
            $('.menu_bot_tab').not($(this)).removeClass('menu_bot_tab_selected');
            $('.container_payment_choices').hide();
        }

        // $(this).css({
        //     'color': 'orange',
        //     'font-weight': 'bold'
        // });

        // $('.menu_bot_tab').not($(this)).css({
        //     'color': '#333333',
        //     'font-weight': 'normal'
        // });

        if(tab == '1') {
            $('.container_menu_current_order').hide();
            $('.container_menu_help').hide();
            $('.container_menu_bill').hide();
        } else if(tab == '2') {
            // Current order
            $('.container_menu_current_order').show();
            $('.container_menu_help').hide();
            $('.container_menu_bill').hide();
        } else if(tab == '3') {
            $('.container_menu_help').show();
            $('.container_menu_bill').hide();
        } else if(tab == '4') {
            $('.container_menu_bill_detail').show();
            let tid = $(this).data('tid');
            $('.container_bill_load').load('/menu/bill/load', {tid:tid}, function(r) {
                console.log(r);
                $('.loader_menu_bill_detail').hide();
            });
        }
    });

    // Menu detail qty checkout
    $('body').delegate('.btn_menu_qty_min_checkout, .btn_menu_qty_plus_checkout', 'click', function(e) {
        e.stopPropagation();
        let total = parseInt(remove_dot($('.checkout_total').text()));

        let btn = $(this).data('btn');
        let parent = $(this).parent();
        let gparent = parent.parent().parent();
        let qty_container = parent.find('.menu_row_qty');
        let qty = qty_container.text();

        let menu_id = parent.find('.menu_row_qty').data('menu_id');
        console.log(menu_id);
        let row_menu = $('.row_menu[data-menu_id="' + menu_id + '"]');
        
        let price = parseInt(remove_dot(gparent.find('.row_menu_price_amt').text()));
        if(btn == '0') {
            if(qty != 1) {
                qty--;
                
            } else {
                gparent.parent().parent().remove();
            }
            total -= parseInt(price);
            row_menu.find('.btn_menu_qty_min').click();
        } else {
            qty++;
            total += parseInt(price);
            row_menu.find('.btn_menu_qty_plus').click();
        }

        console.log(price);
        qty_container.text(qty);
        $('.checkout_total').text(add_dot(total.toString()));
    });

    // Menu detail plus minus
    $('body').delegate('.menu_detail_qty_plus, .menu_detail_qty_min', 'click', function() {
        let btn = $(this).data('btn');
        let parent = $(this).parent();
        let qty_container = parent.find('.menu_detail_qty');
        let qty = parseInt(qty_container.text());
        
        if(btn == '0') {
            if(qty != 1) {
                qty--;
            } else {
                //parent.hide();
                //parent.parent().find('.btn_menu_add').show();
            }
        } else {
            qty++;
        }

        qty_container.text(qty);
    });

    // Btn add to cart
    var arr_cart = [];
    $('body').delegate('.btn_add_to_cart', 'click', function() {
        let menu_id = $(this).data('menu_id');
        let menu_price = $(this).data('menu_price');
        let menu_name = $(this).data('menu_name');

        selected_row = $('.row_menu[data-menu_id="' + menu_id + '"]');
        selected_row.find('.btn_menu_add').hide();
        selected_row.find('.container_row_qty').show();

        let row_qty = parseInt(selected_row.find('.menu_row_qty').text());
        let qty = parseInt($('.menu_detail_qty').text());
        let diff = qty - row_qty;

        if(diff > 0) {
            for(let i = 0; i < diff; i++) {
                selected_row.find('.btn_menu_qty_plus').click();
            }
        } else if(diff < 0) {
            for(let i = 0; i < (diff * -1); i++) {
                selected_row.find('.btn_menu_qty_min').click();
            }
        }

        let checkout_qty = parseInt(remove_dot($('#menu_btn_checkout_qty').text()));
        if(checkout_qty == 0) {
            $('.container_menu_checkout_cart').css({
                'bottom': '-150px'
            });
        } else {
            $('.container_menu_checkout_cart').css({
                'bottom': '60px'
            });
        }

        $('.btn_back_preview').click();
    });

    // Btn checkout
    let menu_scroll_pos = 0;
    $('.btn_checkout').click(function() {
        menu_scroll_pos = $('.menu_container_list').scrollTop();

        $('html, body, .menu_container_top').css({
            overflow: 'hidden',
        });
        
        $('.container_menu_checkout').show();

        let total = 0;
        $('.container_checkout_order').html('');
        
        $('.container_menu_book .menu_row_qty').each(function() {
            let menu_qty = $(this).text();

            if(menu_qty != '0') {
                let menu_id = $(this).data('menu_id');
                let menu_name = $(this).data('menu_name');
                let menu_price = $(this).data('menu_price');
                let menu_img = $(this).data('menu_img');

                console.log(menu_id + ' | ' + menu_qty);
                total += menu_price * menu_qty;

                let order = `<div class='row_menu_checkout'>
                    <div class='row_menu_cimg'>
                        <img src="` + menu_img + `" class='row_menu_img' />
                    </div>

                    <div class='row_menu_r'>
                        <div class='row_menu_name'>` +
                            menu_name +
                        `</div>
                        <div>
                            <input 
                                class='input_menu_notes'
                                type="text"
                                style='
                                    border-top: 0px solid transparent;
                                    border-left: 0px solid transparent;
                                    border-right: 0px solid transparent;
                                    border-bottom: 1px solid #cacaca;
                                    padding: 3px 6px;
                                '
                                placeholder='Catatan'
                            />
                        </div>
                        <div class='row_menu_price'>
                            <div style='flex: 1;'>
                                Rp. <div class='i row_menu_price_amt'>` + add_dot(menu_price.toString()) + `</div>
                            </div>

                            <div style='display: flex; align-items: center;'>
                                <div class='container_row_qty' style='display: inline-block;'>
                                    <div class='btn_menu_qty_min_checkout i'
                                    data-btn='0'>
                                        -
                                    </div>
                                    <div class='i menu_row_qty'
                                    data-menu_id='` + menu_id + `'
                                    data-menu_price='` + menu_price + `'
                                    >` + menu_qty + `</div>

                                    <div class='btn_menu_qty_plus_checkout i' data-btn='1'>+</div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>`;

                $('.container_checkout_order').append(order);

                $('.checkout_total').text(add_dot(total.toString()));
            }
        });
    });

    // Btn submit new order
    $('.btn_submit_new_order').click(function() {
        $('.container_confirm_order').show();
    });

    // Confirmation new order
    $('.container_confirm_order .btn_yes').click(function() {
        let loader = $('.container_menu_loading');
        loader.show();
        $('.container_confirm_order').hide();

        let order_id = $(this).data('order_id');
        let table_id = $(this).data('table_id');
        let p = $(this).data('p');

        let arrOrder = [];
        $('.container_checkout_order').find('.menu_row_qty').each(function() {
            let menu_id = $(this).data('menu_id');
            let menu_qty = $(this).text();
            let menu_price = $(this).data('menu_price');
            let menu_notes = $(this).parent().parent().parent().parent().find('.input_menu_notes').val();
            let order = {
                menu_id: parseInt(menu_id),
                qty: parseInt(menu_qty),
                menu_price: menu_price,
                order_menu_notes: menu_notes,
            };
            arrOrder.push(order);
        });
        
        dataOrders = {"data": arrOrder};
        let jsonOrder = JSON.stringify(dataOrders);
        console.log(jsonOrder);

        $.post('/order/new',
        {
            order_id: order_id,
            table_id: table_id,
            order: jsonOrder
        }, function(r) {
            console.log(r);
            if(r.status == '200') {
                // alert('Pesanan diproses. Mohon menunggu');
                location.href='/menu/t/' + table_id + '?order=finish&p=' + p + '&notif=1';
            } else {
                loader.hide();
                alert('Error. Mohon untuk memanggil waiter.');
            }
        })
    });

    // Confirm order btn no
    $('.container_confirm_order .btn_no').click(function() {
        $('.container_confirm_order').hide();
    });

    // Btn show payment choices
    $('body').delegate('.btn_show_payment_choice', 'click', function() {
        $('.container_confirm_payment').show();
    });

    // Payment choice click
    $('.payment_options_choice2').click(function() {
        let pay = $(this).data('pay');
        $('.input_payment_choice').val(pay);
        
        let label = '';
        if(pay == '1') {
            label = 'OVO';
        } else if(pay == '2') {
            label = 'GoPay';
        } else if(pay == '3') {
            label = 'Kasir';
        }

        $('.btn_payment_select').css({
            'color': '#333333',
        });
        $('.btn_payment_select').text(label);
        $('.container_payment_choices').fadeOut();
    });

    // Btn Payment
    $('.btn_payment').click(function() {
        let order_id = $(this).data('order_id');
        let pay = $('.input_payment_choice').val();

        if(order_id > 0) {
            if(pay != '') {
                $('.container_confirm_payment').show();
            } else {
                // alert('Mohon pilih metode pembayaran');
                $('.btn_show_payment_choice').click();
                loader.hide();
            }
        } else {
            alert('Silahkan membuat pesanan dulu');
        }
    });

    $('.container_confirm_payment .btn_no').click(function() {
        $('.container_confirm_payment').hide();
    });

    $('.container_confirm_payment .btn_yes').click(function() {
        let total = $('.checkout_total').text();
        $('.container_menu_payment_amount').text(total);

        $('.container_payment_choices').fadeIn();
        $('.container_confirm_payment').hide();
    });

    $('.payment_options_choice').click(function() {
        //$('.container_confirm_payment').hide();

        let info = $('.container_confirm_payment .btn_yes');
        let pay = $(this).data('pay');
        let table_id = info.data('table_id');
        let order_id = info.data('order_id');
        //let pay = $('.input_payment_choice').val();
        let amt = parseInt(remove_dot($('.checkout_total').text()));
        console.log(order_id);

        if(order_id > 0) {
            let loader = $('.container_menu_loading');
            loader.show();

            $('.container_payment_confirmation').load('/payment/process',
            {
                table_id: table_id,
                order_id: order_id,
                amt: amt,
                pay: pay,
            }, function(r) {
                console.log(r);
                $('.container_menu_payment').show();
                loader.hide();
            });

            // let arrOrder = [];
            // $('.container_checkout_order').find('.menu_row_qty').each(function() {
            //     let menu_id = $(this).data('menu_id');
            //     let menu_qty = $(this).text();
            //     let menu_price = $(this).data('menu_price');
            //     let order = {
            //         menu_id: parseInt(menu_id),
            //         qty: parseInt(menu_qty),
            //         menu_price: menu_price
            //     };
            //     arrOrder.push(order);
            // });
            
            
            // dataOrders = {"data": arrOrder};
            // let jsonOrder = JSON.stringify(dataOrders);
            // console.log(jsonOrder);

            // $('.container_payment_confirmation').load('/payment/process',
            // {
            //     table_id: table_id,
            //     pay: pay,
            //     amt: amt,
            //     order: jsonOrder,
            // }, function(r) {
            //     console.log(r);
            //     $('.container_payment_confirmation').html(r);
            //     $('.container_menu_payment').show();

            //     // $('.container_payment_finish').html(r);
            //     // $('.checkout-button').click();

            //     loader.hide();
            // });
        } else {
            alert('Silahkan membuat pesanan dulu');
        }
        // $('.container_midtrans_btn').load('/menu/checkoutz');
    });

     // Btn payment option
     $('.btn_payment_option').click(function() {
        let table_id = $(this).data('table_id');
        let pay = $(this).data('pay');
        let amt = parseInt($('.checkout_total').text());

        let arrOrder = [];
        $('.container_checkout_order').find('.menu_row_qty').each(function() {
            let menu_id = $(this).data('menu_id');
            let menu_qty = $(this).text();
            let menu_price = $(this).data('menu_price');
            let order = {
                menu_id: parseInt(menu_id),
                qty: parseInt(menu_qty),
                menu_price: menu_price
            };
            arrOrder.push(order);
        });
        
        
        dataOrders = {"data": arrOrder};
        let jsonOrder = JSON.stringify(dataOrders);
        console.log(jsonOrder);

        $('.container_payment_finish').load('/finish_payment',
        {
            table_id: table_id,
            pay: pay,
            amt: amt,
            order: jsonOrder,
        }, function(r) {
            console.log(r);
            // $('.container_payment_finish').html(r);
            // $('.checkout-button').click();
        });
    });

    // Btn back preview
    $('body').delegate('.btn_back_preview', 'click', function() {
        $('.container_menu_preview').hide();
        $('.container_menu_preview').load('/menu/loader');
    });

    // Btn back page
    $('body').delegate('.btn_back_page', 'click', function() {
        // $(this).parent().hide();
        $('.container_menu_bill_detail').hide();
    });

    // Btn back to menu from bill
    $('body').delegate('.btn_back_to_menu', 'click', function() {
        $('.container_menu_bill_detail').hide();
        $('.menu_bot_tab[data-tab="1"]').click();
    });

    // Btn back checkout
    $('body').delegate('.btn_back_checkout', 'click', function() {
        $('html, body, .menu_container_list').css({
            overflow: 'auto',
        });
        
        $('.menu_container_list').scrollTop(menu_scroll_pos);

        $(this).parent().parent().parent().hide();
        // $('.container_menu_checkout').hide();
    });

    $('body').delegate('.btn_back_payment, .btn_back_payment_choice', 'click', function() {
        $(this).parent().hide();
    });

    // Payment option radio
    $('.payment_btn_choose').click(function() {
        $(this).find('input[name="opt_payment"]').prop('checked', true);
    });

    // Table pass input enter
    $('.input_table_pass').keydown(function(e) {
        if(e.keyCode == 13) {
            $('.btn_submit_table_pass').click();
        }
    });

    // Table password
    $('.btn_submit_table_pass').click(function() {
        let pass = $('.input_table_pass').val();
        let tid = $(this).data('tid');
        let finish = $(this).data('finish');

        if(pass != '') {
            let loader = $('.container_menu_loading');
            loader.show();

            $.post('/table/pass',
            {
                tid: tid,
                pass: pass
            }, function(r) {
                console.log(r);
                if(r.status == 200) {
                    location.href='/menu/t/' + tid + '?order=' + finish + '&p=' + r.p;
                } else {
                    alert('Invalid Password');
                    loader.hide();
                }
            });
        } else {
            alert('Mohon isi password meja');
        }
    });

    // Menu Help
    $('.menu_help_btn').click(function() {
        let tid = $(this).data('tid');
        let msg = $('.menu_help_msg_input').val();

        $(this).hide();
        $('.menu_help_btn_done').show();

        setTimeout(function() {
            $('.menu_help_btn').show();
            $('.menu_help_btn_done').hide();
        }, 5000);

        $.post('/menu/help',
        {
            tid: tid,
            msg: msg,
        }, function(r) {
            console.log(r);
            if(r.Status == 200) {
                $('.menu_help_msg_input').val('');
                ShowNotif();
            }
        });
    });

    function ShowNotif() {
        $('.menu_notification').css({
            'top': '20px'
        });

        setTimeout(function() {
            $('.menu_notification').css({
                'top': '-100px'
            });
        }, 2000);
        // .delay(2000).queue(function (next) {
        //     $(this).css({
        //         'top': '20px'
        //     });   
        // });
    }

});