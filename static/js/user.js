$(function() {

    // Dashboard nav
    $('.user_dashboard_nav').click(function() {
        let nav = $(this).data('nav');
        let mid = $(this).data('mid');

        $.post('/user/navigate',
        {
            mid: mid
        }, function(r) {
            if(r.status == 200) {
                location.href='/merchant/' + nav;
            }
        });
    });

});