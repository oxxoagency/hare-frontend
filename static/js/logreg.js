$(function() {

    $(':input:text:visible').first().focus();

    // Register
    $('.btn_register').click(function() {
        let email = $('.input_email').val();

        if(email != '') {
            let loader = $('.loader1');

            $.post('/register/email',
            {
                email: email
            }, function(r) {
                console.log(r);
                if(r.status == 200) {
                    $('.container_register_verify').show();
                } else {
                    alert("E-mail sudah terdaftar");
                }
            });
        } else {
            alert('Mohon isi e-mail anda');
        }
    });

    // Btn register verified
    $('.btn_register_verified').click(function() {
        let email = $('.input_email').val();
        let pass = $('.input_pass').val();
        let repass = $('.input_repass').val();
        let username = $('.input_username').val();

        if(username != '' && pass != '' && repass != '') {
            if(pass == repass) {
                $.post('/register/username', 
                {
                    email: email,
                    pass: pass,
                    username: username
                }, function(r) {
                    console.log(r);
                    if(r.status == 200) {
                        alert('Pendaftaran berhasil');
                        location.href='/user/'
                    } else {
                        alert('Username tidak tersedia. Silahkan isi username lain');
                    }
                });
            } else {
                alert('Password dan Ulangi Password tidak sama');
            }
        } else {
            alert('Mohon isi lengkap');
        }
    });

    // Login select merchant / branch
    $('.opt_acc').click(function() {
        $('.opt_acc').not($(this)).find('input').attr('checked', false);
        $(this).find('input').attr('checked', true);
        let val = $(this).find('input').val();
        
        if(val == 'm') {
            $('.row_input_merchant_id').hide();
            $('.username_txt_m').show();
            $('.username_txt_b').hide();
        } else {
            $('.row_input_merchant_id').show();
            $('.username_txt_m').hide();
            $('.username_txt_b').show();
        }

        $(this).css({
            'border': '2px solid orange'
        });

        $('.opt_acc').not($(this)).css({
            'border': '1px solid #cacaca'
        });
    });

});