$(function() {
    $('.home_tab_product').click(function() {
        let tab = $(this).data('tab');
        let bid = $(this).data('bid');
        let category = $('.branch_order_option').val();
        let left, status;

        $(this).addClass('home_tab_product_selected');
        $('.home_tab_product').not($(this)).removeClass('home_tab_product_selected');

        if(tab == 1) {
            left = 0;   
            status = 0; 
        } else if(tab == 2) {
            left = 50;
            status = 0;
        } else if(tab == 3) {
            left = 50;
            status = 3;
        }

        $('.home_tab_indicator').css({
            'left': left + '%'
        })

        $('.container_load_branch_order').load('/branch/load_order',
        {
            status: status,
            category: category,
        });
    });

    $('.branch_order_option').change(function() {
        let category = $(this).val();
        let tab = $('.home_tab_product_selected').data('tab');

        if(tab == 1) { status = 0; } 
        else if(tab == 3) { status = 3; }

        eloader1Show();
        $('.container_load_branch_order').load('/branch/load_order',
        {
            status: status,
            category: category,
        }, function() { eloader1Finish(); });
    });

    // Btn pay
    $('body').delegate('.btn_admin_pay', 'click', function() {
        let amt = $(this).data('amt');
        let order_id = $(this).data('order_id');

        $('.lunas_value').text(amt);
        $('.container_confirm_lunas').show();
        $('.container_confirm_lunas .btn_positive').data('order_id', order_id);
    });

    // Btn confirm lunas
    $('body').delegate('.container_confirm_lunas .btn_positive', 'click', function() {
        let loader = $('.hare_loader2');
        $('.container_confirm_lunas').hide();
        loader.show();

        let order_id = $(this).data('order_id');

        $.post('/branch/pay',
        {
            order_id: order_id
        }, function(r) {
            console.log(r);
            if(r.status == 200) {
                $('.home_tab_product[data-tab="2"]').click();
            }
            loader.hide();
        });
    });

    // Btn cancel lunas
    $('body').delegate('.container_confirm_lunas .btn_negative', 'click', function() {
        $('.container_confirm_lunas').hide();
    });

    // Row order menu complete
    $('body').delegate('.row_order_menu', 'click', function() {
        if(!($(this).hasClass('row_order_menu_done'))) {
            let om_id = $(this).data('om_id');
            let menu_name = $(this).find('.order_menu_val_name').text();
            let menu_qty = $(this).find('.order_menu_val_qty').text();

            console.log("omid" + om_id);

            $('.confirm_ordermenu_qty').text(menu_qty);
            $('.confirm_ordermenu_name').text(menu_name);
            $('.container_confirm_menu_update .btn_positive').data('om_id', om_id);
            $('.container_confirm_menu_update').show();
        }
    });

    // Confirm order menu update status
    $('.container_confirm_menu_update .btn_positive').click(function() {
        let om_id = $(this).data('om_id');
        $('.container_confirm_menu_update').hide();

        $.post('/branch/update_order_menu', 
        {
            om_id: om_id
        }, function(r) {
            console.log(r);
            if(r.status == 200) {
                $('.row_order_menu[data-om_id="' + om_id + '"]').addClass('row_order_menu_done');
                $('.home_tab_product[data-tab="1"]').click();

                // Check if all order completed
                
            }
        });
    });

    // Btn cancel lunas
    $('body').delegate('.container_confirm_menu_update .btn_negative', 'click', function() {
        $('.container_confirm_menu_update').hide();
    });

    // Btn order done
    $('body').delegate('.btn_row_order_done', 'click', function() {
        let container = $(this).closest('.container_row_branch_order');

        let count_ordermenu = container.find('.container_order_menu_row').length;
        console.log('totallen', count_ordermenu);
        
        let count_orderdone = container.find('.row_order_menu_done').length;
        console.log('orderdone', count_orderdone);

        if(count_ordermenu == count_orderdone) {
            let order_id = $(this).data('order_id');
            let loader = $('.hare_loader2');
            loader.show();
            
            $.post('/order/done',
            {
                order_id: order_id
            }, function(r) {
                console.log(r);
                if(r.status == 200) {
                    $('.home_tab_product[data-tab="1"]').click();
                }
                loader.hide();
            });
        } else {
            alert('Semua order belum selesai');
        }
    });

});