$(function() {
    $(document).click(function() {
        $('.container_mobile_nav').css({
            'width': '0px'
        });
    });

    // Btn menu
    $('.btn_merchant_mobile_menu').click(function(e) {
        e.stopPropagation();
        $('.container_mobile_nav').css({
            'width': '200px',
        });
    });

    $('.container_mobile_nav').click(function(e) {
        e.stopPropagation();
    });

    // Log out
    $('.btn_logout').click(function() {
        $.post('/logout', function(r) {
            location.href='/login';
        });
    });

});