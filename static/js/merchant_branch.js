$(function() {

    // Branch nav
    $('.branch_nav').click(function() {
        let nav = $(this).data('nav');
        let bid = $(this).data('bid');

        $.post('/merchant/navigate_branch',
        {
            bid: bid
        }, function(r) {
            if(r.status == 200) {
                location.href='/branch';
            }
        });
    });

});