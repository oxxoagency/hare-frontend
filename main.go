package main

import (
    // "net/http"
    "html/template"
    // "log"

	"routes"
	"constants"
	"utils"

	"github.com/gin-gonic/gin"
    // "github.com/gorilla/sessions"
)

var tmpl = constants.TmplRoot
var tmpl_site = constants.TmplSite
var tmpl_common = constants.TmplCommon

func main() {
	r := gin.Default()
	r.SetFuncMap(template.FuncMap{
		"AddTS": util.AddTS,
		"FloatToString": util.FloatToString,
		"IntToString": util.IntToString,
		"ConvertDate": util.ConvertDate,
		"Mul": util.Mul,
		"Add": util.Add,
		"Encrypt": util.EncryptGeneral,
		"CombineVariable": util.CombineVariable,
	})
	// auth.Routes(r)
	
	r.Static("/static", "./static")
	r.LoadHTMLFiles("static/*/*") //  load the static path
	r.LoadHTMLGlob("templates/*/*")

	route.Routes(r)
    r.Run()
}